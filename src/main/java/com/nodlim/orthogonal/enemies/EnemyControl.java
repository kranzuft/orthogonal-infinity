package com.nodlim.orthogonal.enemies;

/**
 * Created by Aden James Prior
 * May 2014
 */

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.common.Entity;
import com.nodlim.orthogonal.common.EntityInterface;
import com.nodlim.orthogonal.common.HotSwapVector;
import com.nodlim.orthogonal.graphics.SpriteSheet;
import com.nodlim.orthogonal.util.Util;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * - Enemies -
 * Function: display player interface and player
 * object, as well as process player-related data
 *
 * Initiated External Class Objects:
 *      list (Linked List)              Class: Enemy
 *      particlesSheet (ArrayList)      Class: SpriteSheet
 */

public class EnemyControl {
    public HotSwapVector<Enemy> enemies; // Holds enemy instances
    public HotSwapVector<Particle> particles; // in-game particles list
    public ArrayList<SpriteSheet> particleSheets; // particle sheet containing images to be loaded for particles list.
    private final SpriteSheet enemyImages; // Images used for enemy

    private final int particleSize = 2; // size of particles loaded from sprite sheet

    private Coords playerCoords = new Coords();

    private int step; // step value

    Offset offset; // Offset

    // Dependent Enemies
    // stores # of enemies that spawned due to scenario
    // events rather than by in-game system.
    private int dependentEnemies;

    public boolean particlesOn = true; // determines whether particles are currently enabled
    private boolean sustainParticles; // determines the length of time particles remain on screen
    private WindowInfo windowInfo;

    /**
     * - Enemies() -
     * Default constructor used
     */

    public EnemyControl(WindowInfo windowInfo) {
        this.windowInfo = windowInfo;
        int enemyTypes = 10;

        offset = new Offset();

        enemies = new HotSwapVector<>();

        particles = new HotSwapVector<Particle>();
        particleSheets = new ArrayList<SpriteSheet>();

        enemyImages = new SpriteSheet("/Enemies.png", 20);

        for (int i = 1; i <= enemyTypes; i++) {
            particleSheets.add(new SpriteSheet(new SpriteSheet("/Enemies12x.png", 12).getBI(i), particleSize));
        }
    }

    public void enoughEnemies() {

    }

    /**
     * - Update -
     * update the enemies object
     * This includes updating particles and enemies
     */

    public void update(double dt, double pdx, double pdy, int px, int py, int step) {
        // Offset update
        offset.update(pdx, pdy);

        // update timer value step
        this.step = step;

        // Update player coordinates
        this.playerCoords.x = px;
        this.playerCoords.y = py;

        // Update Enemies
        for (int i = enemies.size()-1; i >= 0; i--) {
            Enemy e = enemies.get(i); // get next enemy to be updated


            if (!e.exists()) { // in case of enemy destruction
                spawnParticles(e); // Spawn Particles on Death

                enemies.remove(i);

                if (e.type == 3 && dependentEnemies > 0) dependentEnemies--; // Remove enemy if it is not part of the set
            } else {
                e.move(pdx, pdy, dt);
            }
        }

        // Update Particles
        if (particlesOn) { // if particles are actively being updated
            Iterator<Particle> itParticles = particles.iterator();

            while (itParticles.hasNext()) {
                Particle p = itParticles.next();

                if (p.exists) {
                    p.update(dt, pdx, pdy);
                } else {
                    itParticles.remove();
                }
            }
        } else if (particles.size() > 0) {
            particles.clear();
        }
    }

    // Reset Enemies class
    public void reset() {
        particles.clear();
        enemies.clear();
        offset.reset();
    }

    /**
     * - addEnemies -
     * Adds enemies to the game.
     * Called by Game Control.
     */

    public void addEnemies(int level) {
        double group = (Math.random() * 4);
        if (group < 1) { // chance spawn enemy one or two
            if (Math.random() < (1 - level/120f) || level == 1) {
                for (int i = 1; i < 3; i ++){
                    enemies.add(new One(this, windowInfo, enemyImages, playerCoords));
                }
            }
            if (Math.random() < (0.9 - level/120f) || level == 1) {
                enemies.add(new Two(this, windowInfo, enemyImages, playerCoords));
            }
        }
        else if (group < 2) { // chance spawn enemy three or four
            if (Math.random() < 0.7 || level == 5){
                enemies.add(new Three(this, windowInfo, enemyImages, playerCoords));
            }
            if (Math.random() < 0.6 || level == 5) {
                enemies.add(new Four(this, windowInfo, enemyImages, playerCoords));
            }
        }
        else if (group < 3) { // chance spawn enemy five or six
            if (Math.random() < 0.5 || level == 10) {
                enemies.add(new Five(this, windowInfo, enemyImages, playerCoords));
            }
            if (Math.random() < 0.4 || level == 10) {
                enemies.add(new Six(this, windowInfo, enemyImages, playerCoords));
            }
        }
        else { // chance spawn enemy seven or eight
            if (Math.random() < 0.35 || level == 15) {
                enemies.add(new Seven(this, windowInfo, enemyImages, playerCoords));
            }
            if (Math.random() < 0.3 || level == 15) {
                enemies.add(new Eight(this, windowInfo, enemyImages, playerCoords));
            }
        }
    }

    // Particle
    // The particle class is used to create instances
    // of particles and determine their lifetime and
    // expiration date
    public class Particle extends Entity implements EntityInterface {
        protected Image image; // image used by all objects
        private double dx;
        private double dy;

        private double tdx;
        private double tdy;

        private double edx;
        private double edy;

        private final int distance = 40;

        private boolean exists;

        public Particle(int x, int y, int c, int r, Image image) {
            this.x = x + (r * particleSize);
            this.y = y + (c * particleSize);
            this.image = image;

            dx = 2 * (1 - (Math.random() * 2)); // dx in range between +- 2
            dy = 2 * (1 - (Math.random() * 2)); // dy in range between +- 2

            // Find timeout point
            double dl = Math.sqrt(dx*dx + dy*dy);
            edx = dx/dl * distance;
            edy = dy/dl * distance;

            exists = true;
        }

        public void update(double dt, double pdx, double pdy) {
            dx -= 0.005;
            dy -= 0.005;
            x += dx - (pdx * dt);
            y += dy - (pdy * dt);
            tdx += dx;
            tdy += dy;

            boolean forgone = Math.abs(tdx) > Math.abs(edx) || Math.abs(tdy) > Math.abs(edy);
            if (Util.offScreen(windowInfo, x, y) || (forgone && !sustainParticles)) {
                exists = false;
            }
        }

        public boolean exists() {
            return exists;
        }

        public Image image() { return image; }
    }

    // spawnParticles and spawnParticlesProcess
    // spawnParticles determines whether particles should
    // be spawned, and spawnParticlesProcess subsequently generates
    // those particles
    public void spawnParticles(Enemy e) {
        if (particlesOn && (particles.size() < 300 && !sustainParticles) || (particles.size() < 500 && sustainParticles)) {
            spawnParticlesProcess(e.type, e.size, e.x(), e.y());
        }
    }

    public void spawnParticlesProcess(int type, int length, int x, int y) {
        if (length > 12) {
            length = 12;
        }
        int sections = length/particleSize;
        for (int r = 0; r < sections; r += 1) {
            for (int c = 0; c < sections; c += 1) {
                Image i = particleSheets.get(type - 1).get((sections * r) + c + 1);
                particles.add(new Particle(x, y, r, c, i));
            }
        }
    }

    // Set whether to sustain particles or not
    public void setParticleMode() {
        sustainParticles = !sustainParticles;
    }


    // Offset
    // Camera class orienting the enemies around the player and
    // determining the borders off screen that the enemies are prevented from passing.
    public class Offset extends Entity implements EntityInterface {
        protected Image image; // image used by all objects

        public void update(double pdx, double pdy) {
            this.x -= pdx;
            this.y -= pdy;
        }

        public void reset() {
            x = 0;
            y = 0;
        }

        public Image image() { return image; }
    }

    public SpriteSheet enemyImages() {
        return enemyImages;
    }

    public int px() {
        return (int) this.playerCoords.x;
    }

    public int py() {
        return (int) this.playerCoords.y;
    }

    public int step() {
        return step;
    }

    public int dependentEnemies() {
        return dependentEnemies;
    }
}
