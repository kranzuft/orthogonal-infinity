package com.nodlim.orthogonal.util;

/**
 * Created by Aden James Prior
 * Apr 2014
 */

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Colour;
import com.nodlim.orthogonal.common.EntityInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * - Util -
 * Function: Holds functions used by multiple classes
 * in the game.
 *
 * Initiated External Class Objects:
 *      spawnAnimationSheet             Class: ASpriteSheet
 *      trailSheet                      Class: SpriteSheet
 *      fire, collision, collision2     Class: GameSound
 *      EnemyHitSounds(Array List)      Class: GameSound
 */

public class Util {

    // - saveFileAsList -
    // Saves a file, based on an array list of each line to be added to the file.
    // Writes over file, does not append.
    public static void saveFileAsList(ArrayList<String> array, String file) throws Exception {
        FileOutputStream out = null;
        BufferedWriter writer;

        try {
            out = new FileOutputStream(new File(Util.getPath(file)));
            writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            int location = 1;
            for (String s : array) {
                if (location > 20) break;
                writer.append(s).append("\n");
                location ++;
            }
            writer.close();
        }
        finally {
            if (out != null) {
                out.close();
            }
        }
    }

    // - loadFileAsList -
    // Loads a file as an array list of each line of the file
    public static ArrayList<String> loadFileAsList(String file) throws IOException {
        FileInputStream in = null;
        ArrayList<String> list  = new ArrayList<String>();

        try {
            in = new FileInputStream(new File(Util.getPath(file)));
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            while (reader.ready()) {
                String readerLine = reader.readLine();
                if (readerLine != null && !readerLine.isEmpty()) {
                    list.add(readerLine);
                }
            }
        }
        finally {
            if (in != null) {
                in.close();
            }
        }

        return list;
    }

    // Checks if the 'data' folder that comes with the game
    // is currently in place.
    public static boolean dataFolderExists() {
        if (!new File(getPath("/data/players.txt")).exists()) {
            return false;
        }else if (!new File(getPath("/data/scores.txt")).exists()) {
            return false;
        }
        return true;
    }

    // Recreates the original data folder
    // Ensures that the high score system always works
    public static void recreateDataFile() {
        // noinspection ResultOfMethodCallIgnored
        new File((getPath("/data/"))).mkdirs(); // recreate folder
        new File(getPath("/data/players.txt")); // recreate players.txt
        new File(getPath("/data/scores.txt")); // recreate scores.txt

        // Generate Scores
        ArrayList<Long> generatedScores = new ArrayList<Long>();
        long zero = 0;
        generatedScores.add(zero);
        long initialScore = 1000000;
        for (int i = 19; i > 0; i --) {
            generatedScores.add(initialScore);
            initialScore += (initialScore/2 + 1000000);
            initialScore = round(initialScore, 2);
        }

        // Get default files
        ArrayList<String> newScores = new ArrayList<String>();
        ArrayList<String> newPlayers = new ArrayList<String>();

        // Save Resulting lists
        for (int i = 20; i > 0; i--) {
            newScores.add(Long.toString(generatedScores.get(i- 1)));
            newPlayers.add("Level " + (i));
        }
        try {
            saveFileAsList(newPlayers, "/data/players.txt");
            saveFileAsList(newScores, "/data/scores.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // - displayTextCore -
    // Core of Text Display
    // Used by displayTextC and displayText
    private static void displayTextCore(String text, int x, int y, Graphics2D d, Font font, Color color, boolean f) {
        FontMetrics m = d.getFontMetrics(font);
        d.setFont(font);
        d.setColor(color);

        int w;
        if (f) w = m.stringWidth(text);
        else w = 0;

        d.drawString(text, x - w/2, y);
    }

    // - near -
    // Checks if a point is close to
    // another point by a fixed radial distance
    // Used by Player and Enemies classes
    public static boolean near(int x, int y, int x2, int y2, int radius) {
        return (Point2D.distance(x, y, x2, y2) < radius);
    }

    // displayTextC
    // Display text centered to a fixed point
    public static void displayTextC(String text, int x, int y, Graphics2D d, Font font, Color color) {
        displayTextCore(text, x, y, d, font, color, true);
    }


    // displays text from a file, of type .txt
    public static void displayTextFromFile(String file, int x, int oy, int spacing, Font font,
                                    Graphics2D g) throws IOException {
        FileInputStream in = null;
        String Border = "_______________________";
        int location = 0;

        try {
            if (file != null) {
                String path = Util.getPath(file);
                if (path != null) {
                    in = new FileInputStream(new File(path));
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                    Util.displayTextC(Border, x, location * spacing + oy, g, font, Colour.iColorS);
                    location += 2;
                    while (reader.ready()) {
                        String line = reader.readLine();
                        Util.displayTextC(line, x, location * spacing + oy, g, font, Colour.iColorSD);
                        location++;
                    }
                    Util.displayTextC(Border, x, location * spacing + oy, g, font, Colour.iColorS);
                }
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    // displayText
    // Display text starting from left at a fixed point
    public static void displayText(String text, int x, int y, Graphics2D d, Font font, Color color) {
        displayTextCore(text, x, y, d, font, color, false);
    }

    // offScreen (coordinates)
    // Determines whether a point is currently outside the window boundaries
    public static boolean offScreen(WindowInfo windowInfo, double x, double y) {
        return x < 0 || x > windowInfo.windowX() || y < 0 || y > windowInfo.windowY();
    }

    // offScreen (coordinates, distance from screen)
    // Determines whether a point is currently outside the window boundaries
    // by a certain degree
    static boolean offScreen (WindowInfo windowInfo, double x, double y, int size) {
        return x < -size || x > windowInfo.windowX() || y < -size || y > windowInfo.windowY();
    }

    // getImage
    // gets an image in the jar that is read-only
    public static Image getImage(String loc) {
        ImageIcon image = new ImageIcon(Util.class.getResource(loc));
        return  image.getImage();
    }

    // displayObject
    // display an object using the specified graphics component
    public static void displayObject(WindowInfo windowInfo, EntityInterface e, Graphics2D g) {
        if (!offScreen(windowInfo, e.x(), e.y(), e.image().getHeight(null))) {
            g.drawImage(e.image(), e.x(), e.y(), null);
        }
    }

    // getPath
    // Get the path of a directory
    public static String getPath(String s) {
        String path = Util.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPath;
        try {
            decodedPath = URLDecoder.decode(path, "UTF-8");
            return decodedPath.substring(0, decodedPath.lastIndexOf("/")) + s;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long round(long value, int figures) {
        return new BigDecimal(value).round(new MathContext(figures, RoundingMode.HALF_UP)).longValue();
    }
}
