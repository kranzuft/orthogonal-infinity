package com.nodlim.orthogonal.enemies;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.Main;
import com.nodlim.orthogonal.graphics.SpriteSheet;

// Red Enemy
// Movement: Random Direction
// Unique Quality: 2 times normal damage inflicted on player
public class One extends Enemy {

    public One(EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal) {
        super(enemyControl, windowInfo, enemyImages, goal);
        setup();
    }

    public void setup() {
        acceleration = 5 + Math.random() * 15;
        speed = 35;
        type = 1;
        score = 50000;
        image = enemyImages.get(type);
        outsideBorderSpawn(100);
        damage = 20;

        moveToward((int) (Math.random() * windowInfo.windowX()), (int) (Math.random() * windowInfo.windowX()));

    }

    public void move() {
        outOfBounds(enemyControl.offset.x(), enemyControl.offset.y());
    }
}
