package com.nodlim.orthogonal;

import java.awt.*;

public class WindowInfo {
    private int windowX;
    private int windowY;

    public WindowInfo(int windowX, int windowY) {
        this.windowX = windowX;
        this.windowY = windowY;
    }

    public int windowX() {
        return windowX;
    }

    public int windowY() {
        return windowY;
    }

    public void update(Rectangle r) {
        this.windowX = (int) r.getWidth();
        this.windowY = (int) r.getHeight();
    }
}
