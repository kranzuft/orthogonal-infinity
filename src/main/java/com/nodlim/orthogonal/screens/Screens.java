package com.nodlim.orthogonal.screens;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Colour;
import com.nodlim.orthogonal.common.Fonts;
import com.nodlim.orthogonal.graphics.TextField;
import com.nodlim.orthogonal.util.Util;

import java.awt.*;
import java.io.IOException;

public class Screens {
    public static void getHighScoreScreen(Graphics2D g, WindowInfo windowInfo) {
        try {
            Util.displayTextC("High Scores", windowInfo.windowX() / 2, windowInfo.windowY() / 9, g, Fonts.largeFont, Colour.iColorS);
            if (Util.dataFolderExists()) {
                Util.displayTextFromFile("/data/players.txt", windowInfo.windowX() / 3, windowInfo.windowY() / 7, 18, Fonts.highScoreFont2, g);
                Util.displayTextFromFile("/data/scores.txt", windowInfo.windowX() * 2 / 3, windowInfo.windowY() / 7, 18, Fonts.highScoreFont2, g);
            } else {
                Util.recreateDataFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getHelpScreen(Graphics2D bd, WindowInfo windowInfo) {
        Color titleColor = new Color(50, 150, 120);
        Color color = new Color(102, 255, 153);

        Font font = new Font("Impact", Font.PLAIN, 20);

        // Title
        Util.displayTextC("Guide", windowInfo.windowX() / 5, windowInfo.windowY() * 9 / 10, bd, Fonts.largeFont, titleColor);
        Util.displayTextC("Guide", windowInfo.windowX() * 4 / 5, windowInfo.windowY() * 9 / 10, bd, Fonts.largeFont, titleColor);

        // current coordinates for displayed information (changed throughout)
        int xLoc = windowInfo.windowX() / 30;
        int yLoc = windowInfo.windowY() / 30;

        // Firing Information
        Util.displayText("Firing", xLoc, yLoc + 40, bd, Fonts.mediumFont, titleColor);
        Util.displayText("Mouse firing", xLoc, yLoc + 75, bd, font, new Color(42, 200, 100));
        Util.displayText("Toggle fire: left click", xLoc, yLoc + 100, bd, font, color);
        Util.displayText("Aim: mouse", xLoc, yLoc + 125, bd, font, color);

        Util.displayText("Keyboard firing", xLoc + 190, yLoc + 75, bd, font, new Color(42, 200, 100));
        Util.displayText("Use directional keys", xLoc + 190, yLoc + 100, bd, font, color);
        Util.displayText("to aim and fire", xLoc + 190, yLoc + 125, bd, font, color);

        // Movement Information
        yLoc = windowInfo.windowY() * 7 / 26 - 20;

        Util.displayText("Movement", xLoc, yLoc + 40, bd, Fonts.mediumFont, titleColor);
        Util.displayText("UP: 'W' key", xLoc, yLoc + 75, bd, font, color);
        Util.displayText("DOWN: 'S' key", xLoc, yLoc + 100, bd, font, color);

        Util.displayText("LEFT: 'A' key", xLoc + 130, yLoc + 75, bd, font, color);
        Util.displayText("RIGHT: 'D' key", xLoc + 130, yLoc + 100, bd, font, color);

        yLoc = windowInfo.windowY() / 2;

        // Instructions
        Util.displayText("Instructions", xLoc, yLoc, bd, Fonts.mediumFont, titleColor);
        Util.displayText("You are the blue square with a trail. Your", xLoc, yLoc + 40, bd, font, color);
        Util.displayText("aim is to gain the highest score and reach", xLoc, yLoc + 62, bd, font, color);
        Util.displayText("beyond the 20th level. You must fire at", xLoc, yLoc + 84, bd, font, color);
        Util.displayText("enemies to gain points. With enough you", xLoc, yLoc + 106, bd, font, color);
        Util.displayText("will level up. Avoid being hit, for that will", xLoc, yLoc + 128, bd, font, color);
        Util.displayText("lose you points.", xLoc, yLoc + 150, bd, font, color);

        xLoc = windowInfo.windowX() * 10 / 18;
        yLoc = windowInfo.windowY() / 30;

        // Miscellaneous Information
        Util.displayText("Miscellaneous", xLoc, yLoc + 40, bd, Fonts.mediumFont, titleColor);
        Util.displayText("Toggle pausing in current game: 'P' key", xLoc, yLoc + 75, bd, font, color);
        Util.displayText("Toggle particles effect mode: 'L' key", xLoc, yLoc + 100, bd, font, color);
        Util.displayText("Toggle particles: 'N' key (reduces any lag)", xLoc, yLoc + 125, bd, font, color);
        Util.displayText("Toggle the music: 'M' key (reduces any lag)", xLoc, yLoc + 150, bd, font, color);
        Util.displayText("Toggle window resizing: 'R' key", xLoc, yLoc + 175, bd, font, color);
        Util.displayText("Shortcut to play menu: 'enter' key", xLoc, yLoc + 200, bd, font, color);
        Util.displayText("Exit to main menu shortcut: escape key", xLoc, yLoc + 225, bd, font, color);

        yLoc = windowInfo.windowY() / 2;

        // Issues/Warnings
        Util.displayText("Warnings", xLoc, yLoc, bd, Fonts.mediumFont, titleColor);
        xLoc -= 4;
        Util.displayText("- Pressing the caption bar's resize button", xLoc, yLoc + 25, bd, font, color);
        Util.displayText("  may cause problems", xLoc, yLoc + 45, bd, font, color);
        Util.displayText("- The 'data' folder in game directory holds", xLoc, yLoc + 75, bd, font, color);
        Util.displayText("  high scores. Deletion resets scores.", xLoc, yLoc + 95, bd, font, color);
        Util.displayText("- Resizing game window will reset game.", xLoc, yLoc + 125, bd, font, color);
    }

    public static void getPlayScreen(WindowInfo windowInfo, TextField username, Graphics2D bd) {
        Util.displayTextC(username.getText().replaceAll("_", " "), windowInfo.windowX() / 2, windowInfo.windowY() / 2, bd,
                Fonts.largeFont, Colour.iColor
        );
        Util.displayTextC(
                "Press 'enter' when done", windowInfo.windowX() / 2, windowInfo.windowY() / 2 + windowInfo.windowY() / 12,
                bd, Fonts.smallFont, Colour.iColorTP
        );
    }
}
