package com.nodlim.orthogonal.buttons;

import com.nodlim.orthogonal.WindowInfo;

import java.awt.*;

/** Exit Button
 *  Used to exit game on-screen */
public class ExitButton extends com.nodlim.orthogonal.graphics.Button {
    public ExitButton(WindowInfo windowInfo, Image primary, Image secondary, Image primaryPressed, Image secondaryPressed) {
        super(windowInfo);
        setButtonImages(primary, secondary, primaryPressed, secondaryPressed);
        setOriginalCoordinates(windowInfo.windowX() * 3 / 4 - width()/2);
        setText("Exit");
    }

    // update the button
    public void customUpdates() {
        if (activated) {
            System.exit(0);
        }
    }
}
