package com.nodlim.orthogonal.enemies;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.graphics.SpriteSheet;
import com.nodlim.orthogonal.util.Util;

// Pink Enemy
// Movement: Toward player until 150 pixels close to them.
// Unique quality: Very very fast but deflected by player
// unless it is close enough to them
public class Three extends Enemy {
    double s;
    double a;

    public Three (EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal) {
        super(enemyControl, windowInfo, enemyImages, goal);
        setup();
    }

    public void setup() {
        s = 50 + (Math.random() * 45);
        speed = s;
        a = 4 + (Math.random() * 3);
        acceleration = a;
        type = 3;
        score = 8000;
        image = enemyImages.get(type);

        borderSpawn();
    }

    public void move() {
        if (Util.near((int) goal.x, (int) goal.y, x(), y(), 150) && !Util.near((int) goal.x, (int) goal.y, x(), y(), 100)) {
            speed = 30;
            acceleration = 5;
            moveAway((int) goal.x, (int) goal.y);
        }
        else {
            speed = s;
            acceleration = a;
            moveToward((int) goal.x, (int) goal.y);
        }
    }
}
