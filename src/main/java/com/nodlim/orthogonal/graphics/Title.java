package com.nodlim.orthogonal.graphics;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.game.Game;
import com.nodlim.orthogonal.util.Util;

import java.awt.*;

// Title //
// Game Title
public class Title {
    private final Image image = Util.getImage("/title.png");
    private WindowInfo windowInfo;

    int x;
    int y;
    int upperBoundary = -100;

    int lowerBoundary; // boundary where title should be positioned
    int velocity; // speed of title in and out of the screen

    public Title(WindowInfo windowInfo) {
        this();
        this.windowInfo = windowInfo;
        x = windowInfo.windowX()/2 - image.getWidth(null)/2;
        y = windowInfo.windowY()/8 - image.getHeight(null)/2;
    }

    // Constructor
    private Title() {
        lowerBoundary = y;
        velocity = 30;
    }

    // Display Title
    public void display(Graphics2D bd) {
        bd.drawImage(image, x, y, null);
    }

    // Update Title
    public void update(double dt, boolean hasActiveButton) {
        if (Game.inGame) {
            hide(dt);
        } else if (hasActiveButton) {
            hide(dt);
        }
        else if (y < lowerBoundary) y += velocity * dt;
        else if (y > lowerBoundary) y = lowerBoundary;
    }

    // hide title
    public void hide(double dt) {
        if (y > upperBoundary) y -= velocity * dt;
        else if (y < upperBoundary) y = upperBoundary;
    }
}
