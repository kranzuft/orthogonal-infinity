package com.nodlim.orthogonal.enemies;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.graphics.SpriteSheet;
import com.nodlim.orthogonal.util.Util;


// miniEnemy
// Enemy that is spawned by Seven and
// has similar characteristics but is
// a quarter of Enemy Seven's size.
public class miniEnemy extends Enemy {
    boolean wait;

    public miniEnemy(EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal, int x, int y) {
        super(enemyControl, windowInfo, enemyImages, goal);
        setup(x, y);
        wait = true;
    }

    public void setup(int x, int y) {
        speed = 40 + (Math.random() * 5);
        acceleration = 3 + (Math.random() * 7);
        type = 7;
        size = 12;
        image = Util.getImage("/midge.png");

        score = 1996;
        damage = 5;

        coords.dx = (1 -  (Math.random() * 2)) * speed;
        coords.dy = (1 - (Math.random() * 2)) * speed;

        this.coords.x = x;
        this.coords.y = y;
    }

    public void move() {
        moveToward(goal.x, goal.y);
        if (wait && (enemyControl.step() % 5 == 0)) {
            wait = false;
        }
    }

    public void explode() {
        if (!wait) exists = false;
    }
}
