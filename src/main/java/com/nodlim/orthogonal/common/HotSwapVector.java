package com.nodlim.orthogonal.common;

import java.util.ArrayList;

public class HotSwapVector <T> extends ArrayList<T> {
    /**
     * We need to be able to manage an arraylist that doesn't care if order isn't kept after operationgs,
     * hence this method makes time for removal in the arraylist O(n)
     * @param index The index where to remove
     * @return The value at the index
     */
    @Override
    public T remove(int index) {
        T t = get(index);
        set(index, get(size() - 1));
        super.remove(size()-1);
        return t;
    }
}



