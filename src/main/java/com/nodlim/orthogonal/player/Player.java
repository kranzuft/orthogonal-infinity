package com.nodlim.orthogonal.player;

/**
 * Created by Aden James Prior
 * Jan 2014
 */

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Colour;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.common.EntityInterface;
import com.nodlim.orthogonal.common.Fonts;
import com.nodlim.orthogonal.enemies.Enemy;
import com.nodlim.orthogonal.enemies.EnemyControl;
import com.nodlim.orthogonal.game.Game;
import com.nodlim.orthogonal.game.GameSound;
import com.nodlim.orthogonal.graphics.ASpriteSheet;
import com.nodlim.orthogonal.graphics.SpriteSheet;
import com.nodlim.orthogonal.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * - Player -
 * Function: display player interface and player
 * object, as well as process player-related data
 *
 * Initiated External Class Objects:
 *      spawnAnimationSheet             Class: ASpriteSheet
 *      trailSheet                      Class: SpriteSheet
 *      fire, collision, collision2     Class: GameSound
 *      EnemyHitSounds(Array List)      Class: GameSound
 */

public class Player implements EntityInterface {
    protected Image image; // image used by all objects
    ASpriteSheet spawnAnimationSheet; // spawn animation sheet for when player first initialised
    ImageIcon img; // Player final image

    private double fdx, fdy; // change in coordinates relative to frame rate
    private int mx, my; // mouse coordinates

    // Health Values
    private Image healthBlock; // Health Image

    private final int startingHealth = 100; // the health provided at start of game
    private int health; // the current health of the player
    private int displayHealth = health; // currently displayed health

    // Velocity and Acceleration Variables
    private double velocity = 25; // velocity when moving horizontally and vertically
    private double diagonalVelocity = Math.sqrt(velocity * velocity * 0.5); // velocity moving diagonally
    double acc = 4; // acceleration
    public Coords coords = new Coords();

    // Key Press Booleans
    private boolean keyD, keyA, keyW, keyS, keyZ, fireD, fireU, fireL, fireR;

    // Sounds
    private GameSound fire; // sound for player firing bullet

    private GameSound collision = new GameSound("/collision.wav", -25.0f); // sound for collision with enemy
    private GameSound collision2 = new GameSound("/collision.wav", -25.0f); // second sound for same collision

    private ArrayList<GameSound> EnemyHitSounds; // list of sounds for player hitting enemy
    private String[] deathSounds = { // sounds for enemy death
            "/enemy_death.wav",
            "/enemy_death2.wav",
            "/enemy_death3.wav"
    };

    // Score
    private long score, displayScore; // score values, first raw and second processed

    // Bullets
    private LinkedList<Bullet> bullets;
    private Image bulletImage =  Util.getImage("/bullet.png");

    // Death state
    private boolean exists = true;
    public boolean explode;

    // Trail Sheets
    SpriteSheet trailSheet;

    // Trail variables
    private ArrayList<double[]> trail;
    final double trailConstant = 3.2;
    int gD = (int) trailConstant;
    private WindowInfo windowInfo;

    // Setup
    public Player(WindowInfo windowInfo) {
        this.windowInfo=windowInfo;
        spawnAnimationSheet = new ASpriteSheet("/player_spawn.png", 40, true);
        image = spawnAnimationSheet.sheet.get(1);

        trailSheet = new SpriteSheet("/player_trail.png", 24);
        healthBlock = Util.getImage("/health.png");

        fire = new GameSound("/fire.wav", -2.0f);

        coords.x = windowInfo.windowX();
        coords.y = windowInfo.windowY();

        trail = new ArrayList<double[]>();
        bullets = new LinkedList<Bullet>();

        EnemyHitSounds = new ArrayList<GameSound>();
        setEnemyKillSounds(); // get new list of enemy hit sounds
    }

    /**
     * -- Display --
     * Displays all entities on screen in-game
     */

    public void display(Graphics2D g) {
        // Display Ghosts
        displayPlayerTrail(g);

        // Display Player
        if ((exists)) {
            g.drawImage(image, x(), y(), null);
        }

        // Display Bullets
        displayBullets(g);

        // Display Health
        for (int i = 0; i < displayHealth; i++) {
            g.drawImage(healthBlock, i, 50, null);
        }
        Util.displayText(Integer.toString((int) (((double) displayHealth / (double) startingHealth) * 100)),
                displayHealth + 1, 66, g, Fonts.smallFont, Colour.pColor);

        // Display Score
        Util.displayText(Long.toString(displayScore), 2, 40, g, Fonts.smallFont, Colour.pColor);
    }

    /**
     * - update -
     * Update the player.
     * This includes updating of:
     * sounds, score, bullets and health.
     */

    public void update(double dt, boolean nextStep, int step, EnemyControl enemyControl) {
        if (img != null) {
            startSequence(dt, nextStep); // animation for start of game (player forms from small squares)
        } else {
            move(dt); // move the player
            updateBullets(nextStep, enemyControl, dt); // update the bullets and check for collisions
        }

        updatePlayerTrail(); // Update the afterglow that follows the player

        // stop sound when not firing
        if ((keyZ || fireD || fireL || fireR || fireU) && !fire.isActive() && Game.playMusic) {
            fire.commence(false);
        }

        // Control displayScore
        updateScore();

        // If enemy hits player, kill the enemy
        checkIfHitEnemy(enemyControl);

        // Determine if game over and current health
        updateHealth(step, nextStep);
    }

    /**
     * - gameOverUpdate -
     * player updates for when game over
     */

    public void gameOverUpdate(Graphics2D g) {
        trail.clear();

        fdx = 0; // reset fdx
        fdy = 0; // reset fdy

        updateScore(); // update current score

        if (health < displayHealth) {
            displayHealth--;
        }

        if (bullets.size() > 0) bullets.clear();

        Util.displayTextC(
                "Game Over", windowInfo.windowX()/2, windowInfo.windowY() * 3 / 8, g, Fonts.largeFont, Colour.pColor);
        Util.displayTextC(
                "Final score: " + displayScore, windowInfo.windowX() / 2, windowInfo.windowY() * 4 / 9, g, Fonts.smallFont, Colour.pColor);
        Util.displayTextC(
                "Hit escape to exit", windowInfo.windowX() / 2, windowInfo.windowY()/2, g, Fonts.smallFont, Colour.pColorTP);
    }

    /**
     * - updateHealth -
     * Update the health of the player
     */

    public void updateHealth(int step, boolean nextStep) {
        if (health <= 0) {
            health = 0;
            exists = false;
            explode = true;
        } else if (nextStep) {
            if (step % 15 == 0 && health < 100) {
                health++;
            }
            if (health < displayHealth) {
                displayHealth--;
            } else if (health > displayHealth) {
                displayHealth++;
            }
        }
    }

    /**
     * - updateScore -
     * Updates the score of the player
     */

    public void updateScore() {
        double scale = (657072.0 / (double) (windowInfo.windowX() * windowInfo.windowY()));
        if (score == 0) {
            displayScore = 0;
        } else if ((displayScore < (long) (score * scale)) && exists) {
            displayScore += ((long) (score * scale))/50;
        } else {
            displayScore = (long) (score * scale);
        }
    }

    /**
     * - checkIfHitEnemy -
     * Checks if the player has collided with the enemy
     * @param enemyControl: Enemies object. One in every game.
     */

    private void checkIfHitEnemy(EnemyControl enemyControl) {
        for (int i = 0; i < enemyControl.enemies.size(); i++) {
            Enemy e = enemyControl.enemies.get(i);
            if (e.bounds().intersects(bounds())) {
                health -= e.damage();
                e.explode();
                if (!collision.isActive() && Game.playMusic) {
                    collision.commence(false);
                } else if (!collision2.isActive() && Game.playMusic) {
                    collision2.commence(false);
                }
            }
        }
    }

    /**
     * -- Reset --
     * Reset the player.
     * Called at start of game.
     */

    public void reset() {
        coords.x = windowInfo.windowX()/2 - image.getWidth(null)/2;
        coords.y = windowInfo.windowY()/2 - image.getHeight(null)/2;

        coords.dx = 0;
        coords.dy = 0;

        exists = true;
        keyZ = false;

        health = startingHealth;
        displayHealth = health;

        trail.clear();

        score = 0;
        displayScore = 0;

        bullets.clear();

        img = new ImageIcon(this.getClass().getResource("/player.png"));

        spawnAnimationSheet = new ASpriteSheet("/player_spawn.png", 40, false);

    }

    /**
     * -- Player Trail --
     * Trail of player (like an afterglow) that follows him as he moves around the screen.
     * Previous coordinates of player are used to store the location of the trails images.
     */

    private void updatePlayerTrail() {
        trail.add(new double[]{coords.x + fdx/15, coords.y + fdy/15});

        while(trail.size() >= ((gD*4)+2)) {
            trail.remove(0);
        }
    }

    /**
     *  -- Player Start Sequence --
     *  Controls the animation cycle and coordinates of the player during the first few moments of gameplay.
     *  All values not used in the rest of the game are set to null when control is relinquished from the function.
     *  @param dt       current time to complete one update of program.
     *  @param animate  current step.
     */

    private void startSequence(double dt, boolean animate) {
        gD = (int) (trailConstant * (1 / (10 * dt)));
        if (image == null) {
            trail.clear();
            image = img.getImage();
            img = null;
            spawnAnimationSheet = null;
            coords.x += 8;
            coords.y += 8;

        } else {
            image = spawnAnimationSheet.Image(animate);
        }
    }

    /**
     * -- Player Trail Display Function --
     * Displays the player trails.
     * It displays only when the entire array of trails coordinates is filled.
     * @param g     Graphics2D component
     */

    public void displayPlayerTrail(Graphics2D g) {
        int gS = trail.size() - 1;

        if (gS + 1 > gD*4 && img == null) {
            for (int i = 0; i < gS; i++) {
                g.drawImage(trailSheet.get(gS - i), (int) trail.get(gD * i)[0], (int) trail.get(gD * i)[1], null);
            }
        }
    }

    /**
     * -- Bullet Updater --
     * Updates the player's currently fired bullets.
     * Checks whether another bullet object is to be created.
     * @param animate   current step.
     * @param enemyControl   Enemies object containing all enemies
     * @param dt        current time to complete one update of program.
     */

    public void updateBullets(boolean animate, EnemyControl enemyControl, double dt) {
        // Set of instructions controlling the firing of bullets
        if ((keyZ || fireD || fireL || fireU || fireR)) { // checks player is firing
            if (bullets.size() == 0) { // checks whether the list size is 0, if so add new bullet
                addBullet();
            }
            else if (!bullets.get(bullets.size() - 1).charging()) { // checks if last bullet has been fired.
                addBullet(); // if last bullet fired then add new bullet
            }
        }

        // Set of instructions that check if bullet exists, whether speed set and the bullet location
        Iterator<Bullet> itBullets = bullets.iterator();
        while (itBullets.hasNext()) {
            Bullet b = itBullets.next();

            if (Util.offScreen(windowInfo, b.x(), b.y())) {
                itBullets.remove();
                continue;
            }

            for (int a = 0; a < enemyControl.enemies.size(); a++) {
                Enemy e = enemyControl.enemies.get(a);
                if (e.bounds().intersects(b.bounds())) {
                    e.explode();
                    if (Game.playMusic) {
                        for (int i = 1; i < EnemyHitSounds.size(); i++) {
                            int sound = (int) (Math.random() * EnemyHitSounds.size() - 1);
                            GameSound s = EnemyHitSounds.get(sound);
                            if (!s.isActive()) {
                                s.commence(false);
                                break;
                            }
                        }
                    }
                    score += e.score();
                }
            }

            b.update(animate, dt);
        }
    }

    /**
     * -- addBullet --
     * Adds a bullet to bullet list.
     * Depending on what method of firing
     * is used, the bullet constructor call
     * differs.
     */

    public void addBullet() {
        int difX = 0;
        int difY = 0;

        if (keyZ) {
            bullets.add(new Bullet(x(), y()));
        } else {
            if (fireU) difY--;
            if (fireD) difY++;

            if (fireL) difX--;
            if (fireR) difX++;

            bullets.add(new Bullet(x(), y(), difX, difY));
        }

        bullets.get(bullets.size() - 1).controlDirection(mx, my);
    }

    /**
     * -- Bullet Display Function --
     * Displays Bullets.
     * If it has not fired yet than it will be overlaying the player.
     * @param g     Graphics2D component
     */

    public void displayBullets(Graphics2D g) {
        for (Bullet b : bullets) {
            g.drawImage(bulletImage, b.x(), b.y(), null);
        }
    }

    /**
     * -- Player Move Function --
     * Moves the player.
     * Complex due to camera and using decimal acceleration values.
     * @param dt current time to complete one update of program.
     */

    private void move(double dt) {

        // x coordinate manipulation
        // controls the coordinates of player in relation to x;
        if (keyA == keyD) {
            coords.dx = multiKeyMotionProcessing(coords.dx);
        } else if (keyA) {
            coords.dx = singleKeyMotionProcessing(-1, keyW, keyS, coords.dx);
        } else {
            coords.dx = singleKeyMotionProcessing(1, keyW, keyS, coords.dx);
        }

        // y coordinate manipulation
        // controls the coordinates of player in relation to y;
        if (keyW == keyS) {
            coords.dy = multiKeyMotionProcessing(coords.dy);
        } else if (keyW) {
            coords.dy = singleKeyMotionProcessing(-1, keyA, keyD, coords.dy);
        } else {
            coords.dy = singleKeyMotionProcessing(1, keyA, keyD, coords.dy);
        }

        // decimal velocity fix
        coords.dx = decimalFix(coords.dx, keyD, keyA);
        coords.dy = decimalFix(coords.dy, keyW, keyS);

        // Variables used to prevent issue with enemies and other entities that run off player motion
        fdx = coords.dx * dt;
        fdy = coords.dy * dt;

        // Prevention from passing frame boundaries
        fdy = frameBoundaryStop(fdy, image.getHeight(null), coords.y, windowInfo.windowY());
        fdx = frameBoundaryStop(fdx, image.getWidth(null), coords.x, windowInfo.windowX());

        coords.x += fdx;
        coords.y += fdy;

        fdx *= 2;
        fdy *= 2;
    }

    /**
     * - singleKeyMotionProcessing -
     * Controls change in coordinates when single key pressed.
     * @param coefficient   determines whether final value is negative or positive
     * @param keyOne        First key pressed
     * @param keyTwo        Second key pressed
     * @param d             current acceleration direction
     */

    public double singleKeyMotionProcessing(int coefficient, boolean keyOne, boolean keyTwo, double d) {
        d += coefficient*acc;

        if (!keyOne && !keyTwo) {
            if (coefficient *d > velocity) {
                d = coefficient * velocity;
            }
        } else if (coefficient * d > diagonalVelocity) {
            d = coefficient * diagonalVelocity;
        }

        return d;
    }

    /**
     * - multiKeyMotionProcessing -
     * Controls change in coordinates when multiple keys pressed.
     * @param d             current acceleration direction
     */

    public double multiKeyMotionProcessing(double d) {
        if (d < 0) {
            d += acc;
        } else if (d != 0) {
            d -= acc;
        }

        return d;
    }

    /**
     * - decimalFix -
     * Fixes coordinates when invalidly small change in x or y applied
     * @param d             current acceleration direction
     * @param keyOne        First key pressed
     * @param keyTwo        Second key pressed
     */

    public double decimalFix(double d, boolean keyOne, boolean keyTwo) {
        if (!(keyOne || keyTwo)) {
            if (d < acc && d > -acc) {
                d = 0;
            }
        } else if (keyOne && keyTwo) {
            d = 0;
        }

        return d;
    }

    /**
     * - frameBoundaryStop -
     * Prevents player moving off screen
     * @param fd            change in either x or y coordinate
     * @param length        length of player
     * @param coordinate    current x or y coordinate for player
     */

    public double frameBoundaryStop(double fd, int length, double coordinate, int boundary) {
        if (coordinate + fd <= 0 && fd < 0) {
            fd = -coordinate;
        } else if (coordinate + fd >= boundary - length && fd > 0) {
            fd = boundary - length - coordinate;
        }

        return fd;
    }

    /**
     * -- Key Press Notifier Functions --
     * Notifies when certain keys are pressed and released
    */

    public void keyPressed(int key) {
        // Movement key(s) down
        if (key == KeyEvent.VK_A) keyA = true;
        if (key == KeyEvent.VK_D) keyD = true;
        if (key == KeyEvent.VK_W) keyW = true;
        if (key == KeyEvent.VK_S) keyS = true;

        // Fire key(s) down
        if (!keyZ && img == null) {
            if (key == KeyEvent.VK_DOWN) fireD = true;
            if (key == KeyEvent.VK_UP) fireU = true;
            if (key == KeyEvent.VK_LEFT) fireL = true;
            if (key == KeyEvent.VK_RIGHT) fireR = true;
        }
    }

    public void keyReleased(int key) {
        // Movement key(s) up
        if (key == KeyEvent.VK_A) keyA = false;
        if (key == KeyEvent.VK_D) keyD = false;
        if (key == KeyEvent.VK_W) keyW = false;
        if (key == KeyEvent.VK_S) keyS = false;

        // Fire key(s) up
        if (key == KeyEvent.VK_UP) fireU = false;
        if (key == KeyEvent.VK_DOWN) fireD = false;
        if (key == KeyEvent.VK_LEFT) fireL = false;
        if (key == KeyEvent.VK_RIGHT) fireR = false;
    }

    /**
     * -- Mouse Notifier Functions --
     * Notifies when certain events are triggered with mouse
     */

    public void mouseMoved(MouseEvent e) {
        if (e.getX() != mx) {
            mx = e.getX();
            my = e.getY();
        }
    }

    public void mouseReleased() {
        keyZ = !keyZ;
    }

    /**
     * -- setEnemyKillSounds --
     * Creates a list of sounds for when destroying enemy.
     */

    public void setEnemyKillSounds() {
        for (int i = 1; i <= 4; i++) {
            EnemyHitSounds.add(new GameSound(deathSounds[i % 3], -10f));
        }
    }

    /**
     * Call Functions for private values
     */

    public double dx() {
        return fdx;
    }

    public double dy() {
        return fdy;
    }

    public Rectangle bounds() {
        return new Rectangle(x(), y(), 20, 20);
    }

    public boolean exists() {
        return exists;
    }

    public Long score() {
        return displayScore;
    }

    @Override
    public int x() {
        return (int) coords.x;
    }

    @Override
    public int y() {
        return (int) coords.y;
    }

    public Image image() { return image; }
}