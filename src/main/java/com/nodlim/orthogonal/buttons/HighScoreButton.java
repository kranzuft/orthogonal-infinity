package com.nodlim.orthogonal.buttons;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.graphics.Button;
import com.nodlim.orthogonal.screens.Screens;

import java.awt.*;

/**
 * High Scores Button
 * Used to view high scores
 */
public class HighScoreButton extends Button {
    public HighScoreButton(WindowInfo windowInfo, Image primary, Image secondary, Image primaryPressed, Image secondaryPressed) {
        super(windowInfo);
        setButtonImages(primary, secondary, primaryPressed, secondaryPressed);
        setOriginalCoordinates(windowInfo.windowX() * 7 / 12 - width() / 2);
        setText("Scores");
    }

    // Displays the high scores of the player
    public void customDisplay(Graphics2D g) {
        Screens.getHighScoreScreen(g, windowInfo);
    }
}
