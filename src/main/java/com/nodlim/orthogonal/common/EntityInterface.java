package com.nodlim.orthogonal.common;

import java.awt.*;

public interface EntityInterface {
    int x();
    int y();
    Image image();
}
