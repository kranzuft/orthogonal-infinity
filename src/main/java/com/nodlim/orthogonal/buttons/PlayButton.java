package com.nodlim.orthogonal.buttons;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.game.Game;
import com.nodlim.orthogonal.graphics.Button;
import com.nodlim.orthogonal.graphics.TextField;
import com.nodlim.orthogonal.screens.Screens;

import java.awt.*;

/**
 * - Play Button -
 * Used to enter the game and set pre-game options
 */

public class PlayButton extends Button {
    // Username
    private TextField username; // username of player

    public PlayButton(WindowInfo windowInfo, Image primary, Image secondary, Image primaryPressed, Image secondaryPressed) {
        super(windowInfo);
        username = new TextField("Enter Username");
        setButtonImages(primary, secondary, primaryPressed, secondaryPressed);
        setOriginalCoordinates(windowInfo.windowX() / 4 - width() / 2);
        setText("Play");
        username = new TextField("Enter_Username");
    }

    public void keyReleased(int key, String c) {
        if (activated) username.editTextField(key, c);

        if (username.isActive()) {
            Game.inGame = true;
            Game.gameSetup = true;
            username.setActive(false);
            activated = false;
//            activeButton = false;
            hide = true;
        }
    }

    // Event for key press
    public void keyPressed(int key) {
        if (activated) username.editTextField(key);
    }

    // Displays Information regarding username for entry to game.
    public void customDisplay(Graphics2D bd) {
        Screens.getPlayScreen(windowInfo, username, bd);
    }

    public String getCurrentUsername() {
        return username.getText();
    }
}
