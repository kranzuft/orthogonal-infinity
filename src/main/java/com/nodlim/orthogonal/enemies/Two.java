package com.nodlim.orthogonal.enemies;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.graphics.SpriteSheet;

// Light Green Enemy
// Movement: Toward Player
// Unique Quality: Standard enemy that move toward player.
// Slow but fast acceleration, meaning it moves toward player
// very precisely
public class Two extends Enemy {
    public Two(EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal) {
        super(enemyControl, windowInfo, enemyImages, goal);
        setup();
    }

    public void setup() {
        speed = 15 + (Math.random() * 17);
        acceleration = speed * (Math.random());
        type = 2;
        image = enemyImages.get(type);
        score = 99999;

        outsideBorderSpawn(100);
    }

    public void move() {
        double[] v1;

        v1 = separate(enemyControl.enemies, enemyControl.enemies.size());

        coords.x = coords.x + v1[0];
        coords.y = coords.y + v1[1];

        moveToward(goal.x, goal.y);

    }
}
