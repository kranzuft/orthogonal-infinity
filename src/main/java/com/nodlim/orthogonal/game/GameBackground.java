package com.nodlim.orthogonal.game;

/**
 * Created by Aden Prior
 * Jan 2014
 */

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Entity;
import com.nodlim.orthogonal.common.EntityInterface;
import com.nodlim.orthogonal.graphics.SpriteSheet;
import com.nodlim.orthogonal.util.Util;

import java.awt.*;
import java.util.ArrayList;

/**
 * - Game Background -
 * Function: To provide and display
 * the star background used throughout
 * the game and change its behaviour
 * depending on conditions.
 */

public class GameBackground {
    private ArrayList<Star> stars; // moving stars
    private ArrayList<bStar> bStars; // stationary stars
    private SpriteSheet images = new SpriteSheet("/stars.png", 3); // star Images
    private Image backdrop = Util.getImage("/sky.png"); // backdrop image
    WindowInfo windowInfo;

    // Constructor
    public GameBackground(WindowInfo windowInfo) {
        this.windowInfo = windowInfo;
        // Star lists
        stars = new ArrayList<Star>(); // moving stars
        bStars = new ArrayList<bStar>(); // stationary stars

        // produce moving stars (500 moving stars)
        for (int i = 1; i < 500; i++) {
            stars.add(new Star());
        }
        // produce stationary stars (100 background stars)
        for (int i = 1; i < 100; i++) {
            bStars.add(new bStar());
        }
    }

    // - Display Module -
    // Displays all images relating to the background
    public void display(Graphics2D g) {
        g.drawImage(backdrop, 0, 0, null); // Displays image in background

        for (bStar s : bStars) { // Displays the stationary stars
            g.drawImage(s.image(), s.x(), s.y(), null);
        }

        for (Star s : stars) { // Displays the moving stars
            Util.displayObject(windowInfo, s, g);
        }
    }

    // Update
    // updates all stars
    // called by ActionListener in 'Main' class
    public void update(double dt) {
        for (Star s : stars) {
            if (!s.reset) {
                s.update(dt);
            } else {
                s.returnToOrigin();
            }
        }
    }

    // inGameUpdate
    // Updates stars when in-game
    // moves each star a certain degree away from the player
    public void inGameUpdate(double dx, double dy) {
        for (Star s : stars) {
            s.inGameUpdate(dx, dy);
        }
    }

    // Star Class //
    // Class which all moving stars are constructed from
    class Star extends Entity implements EntityInterface {
        protected Image image; // image used by all objects
        private double velocity, velocityGame;      // velocity of star
        private final int maxVelocity = 15;         // maximum velocity of any star
        private boolean reset;                      // value that determines when sent back to original x coordinate

        // Create star at specified location with velocity
        public Star() {
            // instantiate star somewhere on-screen
            y = (int) (Math.random() * 2 * windowInfo.windowY()) - windowInfo.windowY()/2;
            x = (int) (Math.random() * 2 * windowInfo.windowX()) - windowInfo.windowX()/2;

            // set velocity of movement
            velocity = Math.random() * maxVelocity;
            
            // set image based on velocity
            image = images.get((int) (velocity/3) + 1);
            if ((int) velocity % 3 == 0) { // set some stars to different sizes then others
                velocity = Math.random() * maxVelocity;
            }
            velocity += 0.05; // ensure a minimum velocity is reached

            velocityGame = velocity/60; // set velocity in-game
        }

        // Reset Star locations
        // This means the stars are returned to origin, far left of screen
        public void returnToOrigin() {
            x = -windowInfo.windowX()/2;
            y = (int) (Math.random() * 2 * windowInfo.windowY()) - windowInfo.windowY()/2;
            reset = false;
        }

        // Update star when in menus
        // Movement is constantly horizontal and to the right
        public void update(double dt) {
            x += velocity * dt;
            if ((windowInfo.windowX() * 1.5) < x) {
                reset = true;
            }
        }

        // Update the star for when in-game
        // Movement is relative to player
        public void inGameUpdate(double dx, double dy) {
            x += -(dx* velocityGame);
            y += -(dy* velocityGame);
        }

        public Image image() { return image; }
    }

    // Constructor class for all stationary stars
    class bStar extends Entity implements EntityInterface {
        protected Image image; // image used by all objects

        // Create star at random on-screen location without velocity
        public bStar() {
            this.y = (int) (Math.random() * windowInfo.windowY());
            this.x = (int) (Math.random() * windowInfo.windowX());
            image = images.get(1);
        }

        public Image image() { return image; }
    }
}
