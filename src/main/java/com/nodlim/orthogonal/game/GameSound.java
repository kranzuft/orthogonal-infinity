package com.nodlim.orthogonal.game;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import java.net.URL;

/**
 * Created by Aden James Prior
 * May 2014
 */

/**
 * Game Sound
 * Class that allows manipulation of .wav files
 */
public class GameSound extends Thread {
    public Clip clip; // clip is the java-imported .wav listener

    // Constructor
    public GameSound(String loc, float startVolume) {
        setup(loc, startVolume);
    }

    // Setup
    // Creates sound
    public void setup(String loc, float startVolume) {
        try {
            clip = AudioSystem.getClip();

            URL stream = this.getClass().getResource(loc);
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(stream);

            clip.open(inputStream);
            adjustVolume(startVolume);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    // Reinitialise sound
    public void commence(boolean loop) {
        if (loop) clip.loop(Clip.LOOP_CONTINUOUSLY);
        clip.setFramePosition(0);
        clip.start();
    }

    // Stop sound
    public void end() {
        clip.stop();
    }

    // Change volume
    public void adjustVolume(float amount) {
        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        gainControl.setValue(amount);
    }

    // Check if active
    public boolean isActive() {
        return clip.isActive();
    }
}
