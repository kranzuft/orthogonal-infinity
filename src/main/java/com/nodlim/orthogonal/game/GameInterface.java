package com.nodlim.orthogonal.game;

/**
 * Created by Aden James Prior
 * Jan 2014
 */

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.buttons.ButtonsManager;
import com.nodlim.orthogonal.common.Colour;
import com.nodlim.orthogonal.common.Fonts;
import com.nodlim.orthogonal.graphics.Title;
import com.nodlim.orthogonal.util.Util;
import com.nodlim.orthogonal.graphics.Button;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * - Game Interface -
 * Function: Displays buttons and information
 * relevant to the game.
 * <p>
 * Nested Subclasses:
 * Title (title of game)
 * Buttons (buttons for main menu)
 */

public class GameInterface {

    // Objects
    private final Title title; // Title of game object
    private ButtonsManager buttonsManager; // Buttons of game object

    // Sound
    private GameSound buttonPress; // sound for button press
    private GameSound buttonRelease; // sound for button release

    // Boolean
    private boolean inMenu; // determines whether in a menu

    // setup
    boolean showText = true; // determines whether button captions are constantly visible

    // Window Info
    WindowInfo windowInfo;

    // Constructor
    public GameInterface(WindowInfo windowInfo) {
        this.windowInfo = windowInfo;
        title = new Title(windowInfo);
        buttonsManager = new ButtonsManager(windowInfo);
    }

    // Controls updating of objects
    void update(double dt) {
        inMenu = buttonsManager.update(dt);
        title.update(dt, buttonsManager.hasActiveButton());
    }

    // Display all buttons
    void display(Graphics2D bd) {
        title.display(bd);
        for (com.nodlim.orthogonal.graphics.Button b : buttonsManager.getButtons()) {
            if (!Util.offScreen(windowInfo, b.x(), b.y())) {
                b.display(bd, Fonts.smallFont);
            }
        }

        // if in main menu and showText is true, show help text display mode
        if (showText && !inMenu && !Game.inGame) {
            for (com.nodlim.orthogonal.graphics.Button b : buttonsManager.getButtons()) {
                Util.displayTextC(b.text(), b.x() + b.height() / 2, b.y() - 10, bd, Fonts.smallFont, Colour.iColorTPS);
            }
        }
    }

    // enter the play menu
    void enterPlayMenu() {
        buttonsManager.setActive(buttonsManager.get(0));
    }

    // cursor moved and mouse pressed related processes
    void mouseMoved(MouseEvent e) {
        for (com.nodlim.orthogonal.graphics.Button b : buttonsManager.getButtons()) {
            b.accented = b.rect.contains(e.getPoint());
        }
    }

    void mouseClicked(MouseEvent e) {
        buttonsManager.mouseClicked(e);
    }

    // key released and pressed related processes
    void keyReleased(int key, String c) {
        buttonsManager.get(0).keyReleased(key, c);


        if (key == KeyEvent.VK_ESCAPE) {
//            if (dx == 0 && dy == 0) {
            if (!Game.inGame) {
                buttonsManager.resetButtons();
            } else if (inMenu) {
                buttonsManager.setActiveButton(false);
                for (Button b : buttonsManager.getButtons()) {
                    buttonsManager.hideButton(b, false);
                }
            }
//            }
        }
    }

    void keyPressed(int key) {
        buttonsManager.get(0).keyPressed(key);
    }

    public String getUsername() {
        return buttonsManager.getPlayButton().getCurrentUsername();
    }

    public Title getTitle() {
        return title;
    }

    public ButtonsManager getButtonsManager() {
        return buttonsManager;
    }

    public GameSound getButtonPress() {
        return buttonPress;
    }

    public GameSound getButtonRelease() {
        return buttonRelease;
    }

    public boolean isInMenu() {
        return inMenu;
    }

    public boolean isShowText() {
        return showText;
    }

    public WindowInfo getWindowInfo() {
        return windowInfo;
    }
}