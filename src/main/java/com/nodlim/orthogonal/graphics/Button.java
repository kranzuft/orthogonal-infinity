package com.nodlim.orthogonal.graphics;

/**
 * Created by Aden James Prior
 * Jan 2014
 */

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.game.Game;
import com.nodlim.orthogonal.util.Util;

import java.awt.*;

/**
 * - Button -
 * Function: Button super class,
 * holding button-related functions
 * and variables.
 */

public class Button {
    public int ux; // original x location
    public int uy; // original y location

    public boolean accented; // used to define when being focused on for whatever purpose.
    public boolean activated; // expresses the button is clicked on and the menu is activated and by consequence currently displayed
    public boolean hide; //move image off screen

    public Image image, defaultImage, accentedImage, pressedImage, accentedPressedImage; // currently in use image, the image used by default, the accented image
    public String text, defaultText, activeText; // currently in use text, the text used by default, the activated text
    public Rectangle rect; // holds boundaries of button

    public double x, y, dx, dy; // coordinates and vector fields
    protected final WindowInfo windowInfo;
    boolean toggled = false;

    public Button(WindowInfo windowInfo) {
        this.windowInfo = windowInfo;
        this.uy = windowInfo.windowY() * 7 / 8;
    }

    // Set the images used by the button
    public void setButtonImages(Image primary, Image secondary, Image primaryPressed, Image secondaryPressed) {
        defaultImage = primary;
        accentedImage = secondary;
        pressedImage = primaryPressed;
        accentedPressedImage = secondaryPressed;
        image = defaultImage;
        rect = new Rectangle(x(), y(), image.getWidth(null), image.getHeight(null));
    }

    public void togglePressed() {
        if (!toggled) {
            image = pressedImage;
            System.out.println("Button pressed");
        } else {
            image = defaultImage;
            System.out.println("Button unpressed");
        }
        toggled = !toggled;
    }

    public void unpress() {
        image = defaultImage;
    }

    // Set the original coordinates
    public void setOriginalCoordinates(int tx) {
        ux = tx - width() / 2; // center image
        x = ux;
        y = 1100;
    }

    // Set caption text for button
    // Used when hovering over button and
    // when the button is active
    public void setText(String inputText) {
        defaultText = inputText;
        text = inputText;
        activeText = "Back";
    }

    // Update button
    // (Updates all components after initialisation.
    public void update(double dt) {
        if (accented) {
            if (!toggled) {
                image = accentedImage; // changes image depending on user input
            } else {
                image = accentedPressedImage;
            }
        }
        else {
            if (!toggled) {
                image = defaultImage; // switches image to default when input no longer received
            } else {
                image = pressedImage;
            }
        }

        if (activated) { // When activated center button
            move(windowInfo.windowX() / 2 - width() / 2, uy, dt);
            text = activeText;
        } else if ((x != ux) || (y != uy) && !hide) { // If inactive move to initial position
            move(ux, uy, dt);
        }
        // If in-game or setting meant to be off-screen,
        // move off-screen to initial x position
        else if (hide || Game.inGame) {
            move(ux, uy + windowInfo.windowY() / 2, dt);
        }

        // If the image is moving do not change its image.
        // Only change image to highlighted image when button is still
        if (accented && (dx != 0)) accented = false;

        // change rectangle to current coordinates
        rect.setLocation(x(), y());

        // Update coordinates
        x += dx * dt;
        y += dy * dt;

        // Perform any custom updates relative to the button.
        customUpdates();
    }

    // get current text to be displayed for button
    public String text() {
        return text;
    }

    // move button toward location set by locX and locY
    public void move(int locX, int locY, double dt) {
        double procX = (locX - x);
        double procY = (locY - y);
        double angle = Math.atan2(procY, procX);
        if (((dx < 0) && (x < locX)) || ((dx > 0) && (x > locX))) {
            x = locX;
        } else dx += (Math.cos(angle)) * 25 * dt;
        if (((dy < 0) && (y < locY)) || ((dy > 0) && (y > locY))) {
            y = locY;
        } else dy += (Math.sin(angle)) * 25 * dt;
        if ((x == locX) && (y == locY)) {
            dx = 0;
            dy = 0;
        }
    }

    // reset buttons when entering main menu to ensure conditions for
    // scenario are constant
    public void reset() {
        activated = false;
        accented = false;
        hide = false;
        text = defaultText;
    }

    // displays buttons and related text
    public void display(Graphics2D bd, Font commonFont) {
        // display button
        bd.drawImage(Image(), x(), y(), null);
        // display elements
        if (accented || (activated && (dx == 0))) { // if highlighted, or stationary and active
            Util.displayTextC(text(), x() + height() / 2, y() - 10, bd, commonFont, Color.white);
            if (activated && dx == 0) {
                customDisplay(bd);
            }
        }
    }

    // override functions
    public void customDisplay(Graphics2D bd) {
    }

    public void customUpdates() {
    }

    // call functions
    public Image Image() {
        return image;
    }

    public void keyReleased(int key, String c) {
    }

    public void keyPressed(int key) {
    }

    public int x() {
        return (int) x;
    }

    public int y() {
        return (int) y;
    }

    public int width() {
        return image.getWidth(null);
    }

    public int height() {
        return image.getHeight(null);
    }
}
