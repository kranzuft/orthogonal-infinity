package com.nodlim.orthogonal;


/**
 * Created by Aden James Prior
 * Jan 2014
 *
 * -- Overview --
 * The program implements an application that
 * allows the ability to play a simple 2D shoot em' up game
 *
 * @version 1.01
 */

import com.nodlim.orthogonal.game.Game;

import javax.swing.*;
import java.awt.*;

/** - Main -
 * Function: Initialises all components of the game.
 */

public class Main extends JFrame {

    // Serial For Game Version
    private static final long serialVersionUID = 5923117037416870420L;

    // Default frame size values
    private final int START_SCREEN_X = 800;
    private final int START_SCREEN_Y = 600;

    public void run() {
        WindowInfo windowInfo = new WindowInfo(START_SCREEN_X, START_SCREEN_Y);
        // Setup JFrame
        add(new Game(windowInfo)); // Initiates Game panel
        setTitle("Orthogonal Infinity"); // Sets window title
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // allows window to close
        getContentPane().setPreferredSize(new Dimension(START_SCREEN_X, START_SCREEN_Y)); // Set frame initial size
        pack();

        // Set screen width and height
        windowInfo.update(new Rectangle(getContentPane().getWidth(), getContentPane().getHeight()));

        // Setup and set JFrame in relation to the screen
        setMinimumSize(new Dimension(START_SCREEN_X, START_SCREEN_Y)); // Set minimum size of frame
        setLocationRelativeTo(null); // Center on screen
        setVisible(true); // Make Visible
        setResizable(false); // Make initially not resizable
    }

    // - Game - //
    // Function initiates the game when opened
    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }
}
