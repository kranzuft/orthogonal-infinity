package com.nodlim.orthogonal.enemies;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.graphics.SpriteSheet;
import com.nodlim.orthogonal.util.Util;

// Blue Enemy
// Movement: Toward player
// Unique Quality: On destruction spawns two mini
// enemies from class miniEnemy.
public class Seven extends Enemy {
    public Seven (EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal) {
        super(enemyControl, windowInfo, enemyImages, goal);
        setup();
    }

    public void setup() {
        speed = 24 + (Math.random() * 20);
        acceleration = 0.5 + Math.random() * 3;

        type = 7;
        score = 5678;

        image = enemyImages.get(type);

        outsideBorderSpawn(40);
    }

    public void move() {
        moveToward(goal.x, goal.y);

        if (Util.near((int) goal.x, (int) goal.y, x(), y(), 70)) {
            explode();
        }
    }

    public void explode() {
        exists = false;

        for (int i = 1; i <= 2; i++) {
            enemyControl.enemies.add(new miniEnemy(enemyControl, windowInfo, enemyImages, goal, x() + (int) (Math.random() * 10), y()  + (int) (Math.random() * 10)));
        }
    }
}
