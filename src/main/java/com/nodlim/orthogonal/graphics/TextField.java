package com.nodlim.orthogonal.graphics;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Aden James Prior
 * Apr 2014
 */
public class TextField {
    ArrayList<String> textField; // list of elements in text field
    String textString; // string version of elements in text field
    ArrayList<String> acceptedKeys; // keys being accepted by the text field as suitable
    boolean active; // determiner of whether the text field is accepting input

    public TextField(String text) {
        textField = new ArrayList<String>(Arrays.asList((text).split("(?<!^)")));
        textString = getText();

        acceptedKeys = new ArrayList<String>(Arrays.asList(
                ("a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,_," +
                        "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z").split(",")));
    }

    // editTextField
    // allows the editing of the text field by a user
    public void editTextField(int key, String c) {
        // Clear default text on first keystroke
        if (getText().contentEquals(textString)) {
            textField.clear();
        }

        // Prevention of incorrect key appending and prevention of a text field size < 20
        if (textField.size() < 20) {
            if (acceptedKeys.contains(c)) {
                textField.add(c);
            }
            if (key == KeyEvent.VK_SPACE) {
                textField.add("_");
            }
        }

        if (key == KeyEvent.VK_ENTER && !getText().contentEquals("")) active = true; // set to not accepting input
    }

    // editTextField for back space
    // removes element of text field if back space key pressed,
    public void editTextField(int key) {
        if (key == KeyEvent.VK_BACK_SPACE) {
            if (textField.size() != 0) textField.remove(textField.size() - 1);
        }
    }

    // getText
    // Get text currently stored in text field
    public String getText() {
        String string = textField.toString().replaceAll("[\\W]", "");
        string = string.replace(" ", "");
        return string;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
