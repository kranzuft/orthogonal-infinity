package com.nodlim.orthogonal.buttons;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.game.Game;
import com.nodlim.orthogonal.game.GameSound;
import com.nodlim.orthogonal.graphics.Button;
import com.nodlim.orthogonal.graphics.SpriteSheet;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class ButtonsManager {
    private SpriteSheet images = new SpriteSheet("/menu.png", 38);
    private SpriteSheet pressedImages = new SpriteSheet("/menu2.png", 32);
    private boolean activeButton = false;
    GameSound buttonPress = new GameSound("/button_press.wav", -10.0f);
    GameSound buttonRelease = new GameSound("/button_release.wav", -10.0f);

    ArrayList<Button> buttons = new ArrayList<>();
    WindowInfo windowInfo;

    private final PlayButton playButton;
    private final GuideButton guideButton;
    private final HighScoreButton highScoreButton;
    private final ExitButton exitButton;

    public ButtonsManager(WindowInfo windowInfo) {
        this.windowInfo = windowInfo;
        // add buttons and create button list
        // play 0; guide 1; highScore 2; Exit 3; defaultImage = images.getC(1, buttonCode + 1); accentedImage = images.getC(2, buttonCode + 1);
        playButton = new PlayButton(windowInfo,
                images.getCoords(1, 1),
                images.getCoords(2, 1),
                pressedImages.getCoords(1, 1),
                pressedImages.getCoords(2, 1)
        );
        guideButton = new GuideButton(
                windowInfo,
                images.getCoords(1, 2),
                images.getCoords(2, 2),
                pressedImages.getCoords(1, 2),
                pressedImages.getCoords(2, 2)
        );
        highScoreButton = new HighScoreButton(
                windowInfo,
                images.getCoords(1, 3),
                images.getCoords(2, 3),
                pressedImages.getCoords(1, 3),
                pressedImages.getCoords(2, 3)
        );
        exitButton = new ExitButton(
                windowInfo,
                images.getCoords(1, 4),
                images.getCoords(2, 4),
                pressedImages.getCoords(1, 4),
                pressedImages.getCoords(2, 4)
        );

        buttons.add(playButton);
        buttons.add(guideButton);
        buttons.add(highScoreButton);
        buttons.add(exitButton);
    }

    // Updates each button sequentially in same order as button Code's ascend
    public boolean update(double dt) {
        boolean inMenu = false;
        for (com.nodlim.orthogonal.graphics.Button b : buttons) {
            b.update(dt);
            if (!inMenu && b.activated) {
                inMenu = true;
            }
        }
        return inMenu;
    }

    public Button get(int i) {
        return buttons.get(i);
    }

    // Activate the button
    public void activateButton(com.nodlim.orthogonal.graphics.Button b) {
        b.togglePressed();
        b.activated = true;
        b.accented = false;
        activeButton = true;
    }

    // Hide the button
    public void hideButton(com.nodlim.orthogonal.graphics.Button b, boolean state) {
        b.activated = false;
        b.hide = state;
        b.text = b.defaultText;
    }

    // click mouse of button
    public void mouseClicked(MouseEvent e) {
        if (!activeButton) {
            for (com.nodlim.orthogonal.graphics.Button b : buttons) {
                if (b.rect.contains(e.getPoint())) {
                    setActive(b);
//                    showText = false;
                }
            }
        } else {
            for (com.nodlim.orthogonal.graphics.Button b : buttons) {
                if (b.rect.contains(e.getPoint()) && b.activated) {
                    activeButton = false;
                    if (Game.playMusic) {
                        buttonRelease.commence(false);
                    }
                    b.togglePressed();
                    b.x -= 6;
                    b.uy -= 6;
                    b.y -= 6;
                    for (com.nodlim.orthogonal.graphics.Button k : buttons) {
                        hideButton(k, false);
                    }
                }
            }
        }
    }

    // Set button to active
    public void setActive(com.nodlim.orthogonal.graphics.Button b) {
        activateButton(b);
        b.x += 6;
        b.uy += 6;
        b.y += 6;
        if (Game.playMusic) buttonPress.commence(false);
        for (com.nodlim.orthogonal.graphics.Button d : buttons) {
            if (!d.equals(b)) {
                hideButton(d, true);
            }
        }
    }

    // Reset all buttons
    public void resetButtons() {
        activeButton = false;
        for (Button b : buttons) {
            b.reset();
            b.unpress();
            b.uy = windowInfo.windowY() * 7 / 8;
        }
    }

    public ArrayList<Button> getButtons() {
        return buttons;
    }

    public boolean hasActiveButton() {
        return activeButton;
    }

    public void setActiveButton(boolean activeButton) {
        this.activeButton = activeButton;
    }

    public PlayButton getPlayButton() {
        return playButton;
    }
}
