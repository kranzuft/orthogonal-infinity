package com.nodlim.orthogonal.enemies;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.graphics.SpriteSheet;

// Dark Green Enemy
// Movement: Toward Player
// Unique Quality: Highest speed of enemies that move directly toward player
public class Six extends Enemy {
    public Six(EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal) {
        super(enemyControl, windowInfo, enemyImages, goal);
        setup();
    }

    public void setup() {
        speed = 50 + (40 * Math.random()/2);
        acceleration = speed/15;

        type = 6;
        score = 45678;

        image = enemyImages.get(type);

        outsideBorderSpawn(400);
    }

    public void move() {
        moveToward(goal.x, goal.y);
    }
}
