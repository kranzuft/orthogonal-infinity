package com.nodlim.orthogonal.enemies;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.graphics.SpriteSheet;

// Purple Enemy
// Movement: Random Direction
// Unique Quality: Chance to replicate itself every four seconds.
public class Four extends Enemy {
    private boolean justSpawned = true;
    int enemyAdditionCounter=0;

    public Four(EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal) {
        super(enemyControl, windowInfo, enemyImages, goal);
        setup();
        outsideBorderSpawn(100);
    }

    public Four(EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages,  Coords goal, int x, int y) {
        super(enemyControl, windowInfo, enemyImages, goal);
        this.coords.x = x;
        this.coords.y = y;
        setup();
    }

    public void setup() {
        speed = 1.5 + (Math.random());
        type = 4;
        image = enemyImages.get(type);
        score = 50;

        coords.dx = (1 - (Math.random() * 2)) * speed;
        coords.dy = (1 - (Math.random() * 2))  * speed;
    }

    public void move() {
        if (coords.x < windowInfo.windowX() && coords.x > 0 && coords.y < windowInfo.windowY() && coords.y > 0) {
            if (enemyControl.step() / 2 == 50 && ++enemyAdditionCounter == 5 && !justSpawned) {
                enemyControl.enemies.add(new Four(enemyControl, windowInfo, enemyImages, goal, x(), y()));
                enemyControl.spawnParticles(this);
                enemyAdditionCounter = 0;
            }
        }
        justSpawned = false;

        coords.x += coords.dx;
        coords.y += coords.dy;

        regionSwitch();
    }
}
