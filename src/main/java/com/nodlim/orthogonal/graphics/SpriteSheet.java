package com.nodlim.orthogonal.graphics;

/**
 * Created by Aden James Prior
 * Jan 2014
 */

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * - SpriteSheet -
 * Function: provides an array of images from a source
 * image
 */
public class SpriteSheet {
    private BufferedImage sheet; // image to manipulate
    private BufferedImage[] sprites; // image array
    private int col;

    // Constructor using an already provided directory for source image
    public SpriteSheet(String loc, int gridSize) {
        try {
            this.sheet = ImageIO.read(getClass().getResourceAsStream(loc)); // get source image
        } catch (IOException e) {
            e.printStackTrace();
        }
        setup(gridSize);
    }

    // Constructor using an already provided image
    public SpriteSheet(BufferedImage image, int gridSize) {
        this.sheet = image;
        setup(gridSize);
    }

    // Setup
    // Sets up the sprite sheet, using parameters
    // provided by the constructor.
    private void setup(int gridSize) {
        col = sheet.getWidth()/gridSize;
        int row = sheet.getHeight()/ gridSize;
        sprites = new BufferedImage[col * row];
        for (int r = 0; r < row; r++) {
            for (int c = 0; c < col; c++) {
                sprites[(col*r) + c] = sheet.getSubimage(gridSize * c, gridSize * r, gridSize, gridSize);
            }
        }
        sheet = null;
    }

    // Get Image from sprites array
    public Image get(int location) {
        return sprites[location - 1];
    }

    // Get Buffered image from sprites array
    public BufferedImage getBI(int location) {
        return sprites[location - 1];
    }

    // get Image using coordinates
    public Image getCoords(int c, int r) {
        r -= 1;
        c -= 1;

        return sprites[col*r + c];
    }

    // get size of image array
    public int size() {
        return sprites.length;
    }
}