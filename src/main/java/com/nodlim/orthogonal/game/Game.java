package com.nodlim.orthogonal.game;

/**
 * Created by Aden James Prior
 * Jan 2014
 */

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * - Game -
 * Function: governs all processes and subprograms
 * in the program
 *
 * Nested Subclasses:
 *          Key Adapter Class (TAdapter)
 *          Mouse Adapter Class (MAdapter)

 * Initiated External Class Objects:
 *          Interface   Class: GameInterface
 *          Control     Class: GameControl
 *          Background  Class: GameBackground
 */

public class Game extends JPanel implements ActionListener {
    public static boolean inGame = false;
    public static boolean gameSetup = false;

    // Frame Rate and Game Speed
    // These following variables hold last and current time for single process.
    // They modify speed of entities based on frame rate.

    private double pt = -1;     // previous time
    private double dt;          // delta time

    // Backdrop
    public static boolean particlesState = true;

    // Object Containers
    private GameInterface gameInterface;    // Game Interface
    private GameBackground gameBackground;  // Game Background
    private GameControl gameControl;        // Game Control

    // Music
    private GameSound interfaceMusic;   // game theme music
    private GameSound inGameMusic;      // in-game music

    public static boolean playMusic = true;    // boolean controlling whether music playing

    // Control booleans
    public boolean paused;               // boolean determining whether game is paused
    public boolean reset;               // boolean determining whether game needs to be reset
    private boolean resizable;
    private WindowInfo windowInfo;


    public Game(WindowInfo windowInfo) {
        this.windowInfo = windowInfo;
        // Key and Mouse Listeners.
        addKeyListener(new TAdapter());
        addMouseMotionListener(new MAdapter());
        addMouseListener(new MAdapter());

        // JPanel Setup
        setFocusable(true);
        setDoubleBuffered(true);
        setBackground(new Color(5, 10, 25));

        // Initialise classes
        gameInterface = new GameInterface(windowInfo);
        gameBackground = new GameBackground(windowInfo);
        gameControl = new GameControl(windowInfo);

        // Initialise Sounds
        interfaceMusic = new GameSound("/delaware.wav", -10.0f);
        interfaceMusic.commence(true);

        inGameMusic = new GameSound("/in_game.wav", 6.0f);
        inGameMusic.end();

        // Recreate any missing data
        if (!Util.dataFolderExists()) {
            Util.recreateDataFile();
        }

        // Set timer for frame rate
        Timer timer = new Timer(16, this); // approximately 60 fps (around 62.5 to be more precise)
        timer.start();

        // Record current time
        pt = System.nanoTime();
    }

    // Component controlling all displayed components on game window
    @Override
    public void paintComponent(Graphics g) {
        // Set graphics, font and color
        super.paintComponent(g);

        Graphics2D graphics = (Graphics2D) g;

        // Setup Graphics
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.setColor(Color.white);
        graphics.getFontMetrics();

        // display background, control, and interface object components
        gameBackground.display(graphics);
        if (inGame) gameControl.display(graphics);
        gameInterface.display(graphics);

        // Synchronise screen and virtual machine
        Toolkit.getDefaultToolkit().sync();

        // Dispose of screen after display
        graphics.dispose();
    }


    // Function initialising all actions in-game
    public void actionPerformed(ActionEvent e) {
        double ct = System.nanoTime();
        dt = (ct - pt)/1e8;

        // Update in-game components and background
        if (inGame && !paused) {
            updateGame();
        }

        // Update background
        if (!paused) {
            if (inGame) {
                gameBackground.inGameUpdate(gameControl.getPlayer().dx(), gameControl.getPlayer().dy());
            } if (!inGame) {
                gameBackground.update(dt);
            }
        }

        // Update Interface
        gameInterface.update(dt);

        // Following if statement changes the values of screen size when screen is changed in size
        // and ques for reset
        if (getBounds().getHeight() !=  windowInfo.windowY() || getBounds().getWidth() != windowInfo.windowX()) {
            windowInfo.update(getBounds());
            reset = true;
        } else if (reset) { // If que for reset and above is no longer true, reset game
            resetAll();
            reset = false;
        }

        validate(); // lays out components to graphics component
        repaint(); // displays updated graphics component

        pt = ct; // sets new previous time
    }


    /**
     * - resetAll -
     * resets entire program to defaults
     */

    public void resetAll() {
        if (!inGame) {
            gameInterface = new GameInterface(windowInfo);
        } else {
            gameControl = new GameControl(windowInfo);
        }
        gameBackground = new GameBackground(windowInfo);
        gameInterface.update(dt);
        if (inGame) {
            resetGame();
        }
    }

    /**
     * - updateGame -
     * Updates the in-game components and resets them if necessary
     */

    public void updateGame() {
        // Setups up game
        if (gameSetup && inGame) {
            gameControl.reset(gameInterface.getUsername());
            gameSetup = false;
            setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            interfaceMusic.end();
            if (playMusic) inGameMusic.commence(true);
        }

        // Update Game
        gameControl.update(dt);
    }

    /**
     * - resetGame -
     * Resets the game to it's initial settings
     */

    public void resetGame() {
        paused = false;
        inGame = false;
        gameControl.getPlayer().reset();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if (playMusic) {
            inGameMusic.end();
            interfaceMusic.commence(true);
        }
    }

    /**
     * - Key Adapter Class -
     * Handles key events.
     */

    private class TAdapter extends KeyAdapter {
        // Processes for when a key is pressed
        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();

            // Functions to control key presses in-game and in-menu
            gameControl.getPlayer().keyPressed(key);
            gameInterface.keyPressed(key);

            // Interface key press control
            if (!gameInterface.isInMenu()) {
                MainlineKeyProcesses(key);
            }

            // Reset values when exiting in-game to menu
            if (key == KeyEvent.VK_ESCAPE && inGame) {
                resetGame();
            }
        }

        // Processes for when jet is released
        public void keyReleased(KeyEvent e) {
            int key = e.getKeyCode();
            gameControl.getPlayer().keyReleased(key);
            gameInterface.keyReleased(key, Character.toString(e.getKeyChar()));
            keyRelease(key);
        }
    }

    // Interface key press functions
    public void MainlineKeyProcesses(int key) {
        if (key == KeyEvent.VK_N) { // If Key 'N' toggle particles
            gameControl.getEnemyControl().particlesOn = !gameControl.getEnemyControl().particlesOn;
            particlesState = !particlesState;
        }

        // If key 'M' toggle music
        else if (key == KeyEvent.VK_M) {
            playMusic = !playMusic;
            if (playMusic) {
                if (inGame) inGameMusic.commence(true);
                else interfaceMusic.commence(true);
            } else {
                inGameMusic.end();
                interfaceMusic.end();
            }
        }

        // if key 'P' toggle paused
        else if (key == KeyEvent.VK_P && inGame) {
            paused = !paused;
        }
    }

    // Interface key release functions
    public void keyRelease(int key) {
        if (key == KeyEvent.VK_ENTER && !inGame && !gameInterface.isInMenu()) {
            gameInterface.enterPlayMenu();
        }

        // if key 'R' toggle resizing of window
        else if (key == KeyEvent.VK_R) {
            Frame frame = Frame.getFrames()[0];
            if (!resizable) {
                frame.setResizable(true);
                resizable = true;
            } else {
                frame.setResizable(false);
                resizable = false;
            }
        }

        // if key 'L' toggle particle mode
        else if (key == KeyEvent.VK_L && inGame) {
            gameControl.getEnemyControl().setParticleMode();
        }
    }

    /**
     * - Mouse Adapter Class -
     * Handles cursor motion and mouse presses.
     * Only required outside main game.
     */

    private class MAdapter extends MouseAdapter {
        // handles cursor movement
        public void mouseMoved (MouseEvent e) {
            if (!inGame) gameInterface.mouseMoved(e);
            if (inGame) gameControl.getPlayer().mouseMoved(e);
        }

        // handles mouse presses
        public void mouseClicked (MouseEvent e) {
            if (!inGame) gameInterface.mouseClicked(e);
        }

        public void mouseReleased (MouseEvent e) {
            gameControl.getPlayer().mouseReleased();
        }
    }
}
