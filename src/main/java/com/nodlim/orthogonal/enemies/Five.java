package com.nodlim.orthogonal.enemies;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.graphics.SpriteSheet;

import java.awt.*;

// Golden Enemy
// Movement: Toward Player
// Unique Quality: Requires eight hits before
// being destroyed, as each hit only makes it
// smaller until that point. Also worth a lot
// of points.
public class Five extends Enemy {
    private int hits;
    private int timesHit;

    public Five(EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal) {
        super(enemyControl, windowInfo, enemyImages, goal);
        setup();
    }

    public void setup() {
        speed = 30 + (Math.random() * 5);
        acceleration = 0.5 + Math.random() * 3;

        hits = 3;
        timesHit = 0;
        type = 8;
        score = 10000;

        image = enemyImages.get(type);

        outsideBorderSpawn(40);
    }

    public void move() {
        moveToward((int) goal.x, (int) goal.y);

        if (hits > timesHit) {
            timesHit++;
        }
    }

    public void explode() {
        if (hits == 8) {
            exists = false;
        } else {
            hits ++;
            image = image.getScaledInstance(size-hits, size-hits, Image.SCALE_SMOOTH);
        }
    }
}
