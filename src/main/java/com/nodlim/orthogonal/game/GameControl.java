package com.nodlim.orthogonal.game;

/**
 * Created by Aden James Prior
 * Jan 2014
 */

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Colour;
import com.nodlim.orthogonal.common.Fonts;
import com.nodlim.orthogonal.enemies.EnemyControl;
import com.nodlim.orthogonal.player.Player;
import com.nodlim.orthogonal.util.Util;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * - Game Control -
 * Function: to manage in-game processes
 *
 * Initiated External Class Objects:
 *      player      Class: Player
 *      enemies     Class: Enemies
 */

public class GameControl {

    private Player player; // Player object, being the user controlled object
    private EnemyControl enemyControl; // Object controlling all enemies
    private boolean nextStep; // current step being undertaken by game
    private int step, level; // number of steps passed in four second cycle and current level reached in game.
    private long nextLevel = 1000000; // current score needed to level up.
    private boolean leveling; // boolean determining whether currently leveling up
    private String username; // username of player
    private final GameSound levelUp = new GameSound("/levelUp.wav", -20f); // Leveling up sound
    private final WindowInfo windowInfo;

    public GameControl(WindowInfo windowInfo) {
        this.windowInfo = windowInfo;
        player = new Player(windowInfo);
        enemyControl = new EnemyControl(windowInfo);

        step = 1;
        level = 1;

        // Schedule Timers
        setTimers();
    }

    // - Update -
    // updates components of GameControl
    // Initialised by Game
    public void update(double dt) {
        // Update Player
        if (player.exists()) {
            player.update(dt, nextStep, step, enemyControl);
        }

        // Update Enemies
        enemyControl.update(dt, player.dx(), player.dy(), player.x(), player.y(), step);

        // spawn enemy ai
        spawnEnemies();

        // Reset step boolean to false
        if (nextStep) {
            nextStep = false;
        }

        // In case player doesn't exist
        if (!player.exists()) {
            deathCycle();
        }
        // Control current level
        levelControl();
    }

    // - levelControl -
    // Procedure to update values when reaching next level of game
    public void levelControl() {
        // Check if reached next level
        if (player.score() > nextLevel) {
            level++;
            leveling = true;

            // Set kills required
            player.setEnemyKillSounds();

            // Set next goal to win
            nextLevel += (nextLevel/2 + 1000000);
            nextLevel = Util.round(nextLevel, 2);

            if (!levelUp.isActive() && Game.playMusic) {
                levelUp.commence(false);
            }
        }
    }

    // spawnEnemies
    // Determines whether enemies should be spawned and when to spawn them based on number of enemies on screen
    public void spawnEnemies() {
        int maxEnemies = (int) ((double) (10 + (level * 2)) * ((double) (windowInfo.windowX()* windowInfo.windowY())/(double)480000));
        if ((enemyControl.enemies.size() < maxEnemies && (step % 10 == 0) && !leveling && enemyControl.particles.size() < 150)) {
            enemyControl.addEnemies(level); // spawn enemies
        } else if (leveling) { // Do leveling procedures, ONLY when the enemies have stopped spawning
            levelingProcedure();
        }
    }

    // leveling Procedure
    // Procedure followed when incrementing levels
    // Procedure requires several cycles through game loop, may take a few seconds.
    public void levelingProcedure() {
        if (step % 2 == 0 && nextStep) {
            if (enemyControl.enemies.size() > 0) {
                enemyControl.enemies.get(0).explode();
            } else {
                System.out.println("Finished levelling");
                leveling = false;
            }
        }
    }

    // Display:
    //      enemies
    //      player
    //
    public void display(Graphics2D bd) {
        for (int i = 0; i < enemyControl.enemies.size(); i++) {
            if (enemyControl.enemies.get(i).exists()) {
                Util.displayObject(windowInfo, enemyControl.enemies.get(i), bd);
            }
        }

        for (EnemyControl.Particle p : enemyControl.particles) {
            if (p.exists()) {
                Util.displayObject(windowInfo, p, bd);
            }
        }

        if (leveling) {
            Util.displayTextC("Level " + level, windowInfo.windowX()/2, windowInfo.windowY()/2, bd, Fonts.levelUp, Colour.pColorTP);
        }

        player.display(bd);

        if (!player.exists()) {
            player.gameOverUpdate(bd);
        }

        Util.displayText(Integer.toString(level), windowInfo.windowX()/2, 50, bd, Fonts.controlFont, Colour.pColorTP);
    }

    // setTimers
    // schedules two timers, each going off at separate times.
    // The first is a boolean, the second a step counter of values between 1 and 100.
    // Each is used for a variety of functions

    public void setTimers() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                nextStep = true;
            }
        }, 40, 40);
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                step++;
                if (step == 101) {
                    step = 1;
                }
            }
        }, 40, 40);
    }

    // Cycling processes to mimic player movement
    // Used to keep enemies moving around screen when player is
    // destroyed.
    public void deathCycle() {
        if (player.explode) {
            // Player explosion particles
            enemyControl.spawnParticlesProcess(10, 16, player.x(), player.y());
            enemyControl.spawnParticlesProcess(7, 16, player.x(), player.y());
            // Save current score to scores.txt and players.txt
            saveHighScore();
            player.explode = false;
        }

        // Change player position, allowing enemies to chase the position
        if (step % 10 == 0) {
            if (Math.random() < 0.5) {
                if (Math.random() < 0.5) {
                    player.coords.y = -100;
                }
                else {
                    player.coords.x = windowInfo.windowX()*(5/4f);
                    player.coords.y = Math.random() * windowInfo.windowY()*5/4 - 50;
                }
            }
            else {
                if (Math.random() < 0.5) {
                    player.coords.y = - 100;
                }
                else {
                    player.coords.x = Math.random() * windowInfo.windowX()*5/4 - 50;
                    player.coords.y = windowInfo.windowY()*(5/4f);
                }
            }
        }
    }

    // - Save High Score -
    // Saves current high score at end of game
    public void saveHighScore() {
        ArrayList<String> scores = new ArrayList<String>(); // scores list to be manipulated
        ArrayList<String> players = new ArrayList<String>(); // players list to be manipulated
        try {
            scores = Util.loadFileAsList("/data/scores.txt"); // load into scores an array list of scores from file
            players = Util.loadFileAsList("/data/players.txt"); // load into players an array list of players from file
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (scores.size() >= 0) { // add score if size > 0
            int scoreSize = scores.size();

            for (int s = 0; s < scoreSize; s++) {
                Long l = Long.parseLong(scores.get(s));
                if (l < player.score()) {
                    scores.add(s, Long.toString(player.score()));
                    players.add(s, username);
                    break;
                }
            }
        } else { // default to adding scores and players if file is empty
            scores.add(Long.toString(player.score()));
            players.add(username);
        }

        try {
            Util.saveFileAsList(scores, "/data/scores.txt");
            Util.saveFileAsList(players, "/data/players.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // reset
    // Resets the game
    public void reset(String username) {
        enemyControl.reset();
        player.reset();
        level = 1;
        this.username = username;
        nextLevel = 1000000;
    }

    public String getUsername() {
        return username;
    }

    public Player getPlayer() {
        return player;
    }

    public EnemyControl getEnemyControl() {
        return enemyControl;
    }
}
