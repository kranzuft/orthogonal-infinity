package com.nodlim.orthogonal.common;

import java.awt.*;

public class Colour {

    // Colors
    // These are all the colour used in the game, with a few
    // exceptions
    // Many colours are used over multiple classes, and storing
    // them here allows quick access to them.

    public static Color pColor = new Color(105, 200, 255);
    public static Color pColorTP = new Color(105, 200, 255, 120);
    public static Color iColor = new Color(105, 200, 255);
    public static Color iColorTP = new Color(105, 200, 255, 150);
    public static Color iColorS = new Color(124, 39, 132);
    public static Color iColorSD = new Color(144, 15, 145);
    public static Color iColorSDD = new Color(150, 50, 250, 200);
    public static Color iColorTPS = new Color(105, 200, 255, 80);
}
