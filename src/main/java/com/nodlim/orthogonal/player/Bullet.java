package com.nodlim.orthogonal.player;

import com.nodlim.orthogonal.common.Entity;
import com.nodlim.orthogonal.common.EntityInterface;

import java.awt.*;

/**
 * Created by Aden James Prior
 * May 2014
 */

/**
 * Bullet Class
 * creates a bullet and sends it in direction
 * specified by user input
 */


public class Bullet extends Entity implements EntityInterface {
    protected Image image; // image used by all objects
    // Common Variables
    private boolean charging = true; // determines whether ready to move yet

    private double dx;  // change in x coordinate.
    private double dy;  // change in y coordinate.

    private boolean noKey = false; // determines whether a key is pressed
    private double keyX; // determines if key directs bullet to left or right
    private double keyY; // determines if key directs bullet upward or downward.

    private int speed; // speed of bullet

    public int timer = 5; // time before bullet fired.

    // Constructor without key direction
    public Bullet (int x, int y) {
        setup(x, y);
        noKey = true;
    }

    // Constructor with key direction
    public Bullet (int x, int y, int keyX, int keyY) {
        setup(x + 5, y + 5);
        this.keyX = keyX;
        this.keyY = keyY;
    }

    // Setup Function
    public void setup(int x, int y) {
        setXY(x + 5, y + 5);

        speed = 120;
    }

    // Update
    // updates the bullet by moving it
    // on the screen
    public void update(boolean animate, double dt) {
        if (charging) {
            if (animate) timer--;
            if (timer == 1) {
                charging = false;
                timer = -1;
            }
        }
        x += dx * dt;
        y += dy * dt;
    }

    // Functions controlling direction of bullet.
    // It decides whether to use mouse input or key input
    public void controlDirection(double mx, double my) {
        if (noKey) {
            processDirection(mx, my);
        }
        else {
            processDirection(keyX + x, keyY + y);
        }
        keyX = 2;
    }

    // Function that processes direction for controlDirection()
    // The function finds the angle of trajectory for bullet
    // and multiplies the magnitude by the bullets speed.
    public void processDirection(double mx, double my) {
        double angle = Math.atan2(my - y, mx - x);
        dx = Math.cos(angle) * speed;
        dy = Math.sin(angle) * speed;
    }

    // setXY
    // sets the coordinates for the bullet
    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Call functions

    public Rectangle bounds() {
        return new Rectangle(x(), y(), 12, 12);
    }

    public boolean charging() {
        return charging;
    }

    public Image image() { return image; }
}
