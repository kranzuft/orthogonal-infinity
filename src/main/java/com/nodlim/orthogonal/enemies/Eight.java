package com.nodlim.orthogonal.enemies;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.graphics.SpriteSheet;

// Orange Enemy
// Movement: Toward player
// Unique Quality: Moves toward player in two stage
// speed cycles (travels for short period at high speed).
public class Eight extends Enemy {
    public Eight(EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal) {
        super(enemyControl, windowInfo, enemyImages, goal);
        setup();
    }

    public void setup() {
        score = 6666 * 6;
        acceleration = 0.8 + (Math.random() * 4);
        speed = 15 + (Math.random() * 20);
        uSpeed = speed;
        type = 5;
        image = enemyImages.get(type);
        outsideBorderSpawn(100);
    }

    public void move() {
        moveToward(goal.x, goal.y);

        if (enemyControl.step() < 30) {
            speed = uSpeed * 4;
            image = enemyImages.get(9);
        } else {
            speed = uSpeed;
            image = enemyImages.get(type);
        }

    }
}
