# README #

### What is this repository for? ###

This is a top-down shooter video game, similar to asteroids, but with a much more modern design, and having a much more varied and dynamic collection of enemyControl.

### How do I get set up? ###

Download the Java Development Kit (JDK 7.0+) and IntelliJ IDEA (Community Edition or better), and open the project in IntelliJ IDEA. 
Otherwise download the jar file, mac bundle, or windows executable, which can be found in the Downloads folder. Java is bundled with the mac bundle, but you will have to install java yourself if you wish to run the windows executable.