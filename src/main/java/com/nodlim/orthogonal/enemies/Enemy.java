package com.nodlim.orthogonal.enemies;

/**
 * Created by Aden James Prior
 * Jan 2014
 */

import com.nodlim.orthogonal.*;
import com.nodlim.orthogonal.common.Coords;
import com.nodlim.orthogonal.common.EntityInterface;
import com.nodlim.orthogonal.graphics.SpriteSheet;
import com.nodlim.orthogonal.util.Util;

import java.awt.*;
import java.util.List;

/**
 * - Enemy -
 * Function: super class for enemy objects,
 * used entirely through java inheritance.
 */

public class Enemy implements EntityInterface {
    protected double speed, uSpeed, acceleration; // movement variables
    protected boolean exists = true; // variable determines whether alive or not
    protected int type, score; // variables determining character and point value respectively
    protected double scale;

    protected int size = 20; // Size of Enemy
    protected int damage = 10; // Damage inflicted on player by enemy on collision.

    final int flockMagnitude = 100; // distance needed to begin flock processes (see below code)
    protected Image image; // image used by all objects
    public Coords coords = new Coords();
    public Coords goal;
    protected EnemyControl enemyControl;
    protected SpriteSheet enemyImages;
    protected final WindowInfo windowInfo;

    // Constructor
    protected Enemy(EnemyControl enemyControl, WindowInfo windowInfo, SpriteSheet enemyImages, Coords goal) {
        this.windowInfo = windowInfo;
        this.enemyImages = enemyImages;
        this.goal = goal;
        scale = (double) (windowInfo.windowX() * windowInfo.windowY()) / 1500000.0; // proportional value to frame size
        speed = speed * scale;
        acceleration = speed * scale;
        this.enemyControl = enemyControl;
    }

    // Moves enemy toward location
    public void move(double dx, double dy, double dt) {
        move();
        this.coords.x += (this.coords.dx * dt) - dx;
        this.coords.y += (this.coords.dy * dt) - dy;
    }

    // moves enemy towards coordinates
    public void moveToward(double x, double y) {
        moveToward(x, y, acceleration, speed);
    }

    // moveToward shared processing function
    public double[] moveTowardValueProcessing(double x, double y, double acceleration) {
        double X = (x - this.coords.x);
        double Y = (y - this.coords.y);
        double angle = Math.atan2(Y, X);

        double changeX = Math.cos(angle) * acceleration;
        double changeY = Math.sin(angle) * acceleration;

        return new double[] {changeX, changeY};
    }

    // move toward value at acceleration and speed variables
    public void moveToward(double x, double y, double acceleration, double speed) {
        double[] change = moveTowardValueProcessing(x, y, acceleration);
        this.coords.dx += change[0];
        coords.dy += change[1];

        applySpeedToDxDy(speed);
    }

    // move Away from coords.x and y
    public void moveAway(int x, int y) {
        double [] change = moveTowardValueProcessing(x, y, acceleration);
        coords.dx -= change[0] * 1.1;
        coords.dy -= change[1] * 1.1;

        applySpeedToDxDy(speed);
    }

    public void applySpeedToDxDy(double speed) {
        if (coords.dx < -speed || coords.dx > speed) {
            coords.dx = speed * (coords.dx/Math.abs(coords.dx));
        }
        if (coords.dy < -speed || coords.dy > speed) {
            coords.dy = speed * (coords.dy/Math.abs(coords.dy));
        }
    }

    // Spawn enemy at border of game.
    public void borderSpawn() {
        if (Math.random() < 0.5) {
            if (Math.random() < 0.5) coords.x = -image.getWidth(null);
            else coords.x = windowInfo.windowX()*5/4;
            coords.y = Math.random() * windowInfo.windowY()*5/4 - 50;
        }
        else {
            if (Math.random() < 0.5) coords.y = -image.getHeight(null);
            else coords.y = windowInfo.windowY()*5/4;
            coords.x = Math.random() * windowInfo.windowX()*5/4 - 50;
        }
    }

    // Spawn enemy outside border
    public void outsideBorderSpawn(int distance) {
        if (Math.random() < 0.5) {
            if (Math.random() < 0.5) coords.x = - distance - (Math.random() * distance);
            else coords.x = windowInfo.windowX() + distance + (Math.random() * distance);
            coords.y = Math.random() * windowInfo.windowY()*5/3 - 200;
        }
        else {
            if (Math.random() < 0.5) coords.y = -distance - (Math.random() * distance);
            else coords.y = windowInfo.windowY() + distance + (Math.random() * distance);
            coords.x = Math.random() * windowInfo.windowX()*6/4 - 200;
        }
    }

    // Spawn enemy outside range
    public void outOfRange(int xBoundary, int yBoundary, int dif1, int dif2, int dif3, int dif4) {
        int minXBound = xBoundary + dif1;
        int minYBound = yBoundary + dif2;
        int maxXBound = xBoundary + dif3;
        int maxYBound = yBoundary + dif4;
        int side = image.getWidth(null);

        if ((coords.x < minXBound) || (maxXBound < coords.x)) {
            coords.dx = -coords.dx;
            if (minXBound > coords.x) coords.x = minXBound;
            else if (maxXBound < coords.x + side) coords.x = maxXBound;
        }
        else if ((minYBound > coords.y) || (maxYBound < coords.y)) {
            coords.dy = -coords.dy;
            if (minYBound > coords.y) coords.y = minYBound;
            if (maxYBound < coords.y + side) coords.y = maxYBound;
        }
    }

    // stop enemy moving out of bounds.
    public void outOfBounds(int x, int y){
        outOfRange(x, y, -392, -292, 380 + windowInfo.windowX(), 280 + windowInfo.windowY());
    }

    // Switch which side of board enemy is on.
    public void regionSwitch() {
        if (coords.x + image.getWidth(null) > windowInfo.windowX() + 100)  coords.x =  - 50;
        else if (coords.x < -100) coords.x = windowInfo.windowX() + 50;

        if (coords.y + image.getHeight(null) > windowInfo.windowY() + 100) coords.y = - 50;
        else if (coords.y < -100) coords.y = windowInfo.windowY() + 50;
    }

    // Separate enemies from each other
    public double[] separate(List<Enemy> list, int enemyCount) {
        int tx = 0;
        int ty = 0;

        for (int i = 0; i < enemyCount; i++) {
            Enemy e = list.get(i);
            if (!this.equals(e) && Util.near(e.x(), e.y(), x(), y(), 16) && e.type == this.type) {
                tx -= e.coords.x - x();
                ty -= e.coords.y - y();
            }
        }

        return new double[]{tx/(flockMagnitude * 0.1), ty/(flockMagnitude * 0.1)};
    }

    // Call functions

    public Rectangle bounds() {
        return new Rectangle(x(), y(), size, size);
    }

    public void explode() {
        exists = false;
    }

    public boolean exists() {
        return exists;
    }

    public int damage() {
        return damage;
    }

    public void setup() {}
    public void move() {}

    public int score() {
        return score;
    }

    public int x() {
        return (int) coords.x;
    }

    public int y() {
        return (int) coords.y;
    }

    public Image image() { return image; }
}
