package com.nodlim.orthogonal.common;

import java.awt.*;

public class Fonts {
    // Fonts
    // These are all the fonts used in the game, with a few
    // exceptions
    // Many fonts are used over multiple classes, and storing
    // them here allows quick access to them.

    public static Font smallFont = new Font("Impact", Font.PLAIN, 18);
    public static Font mediumFont = new Font("Impact", Font.PLAIN, 30);
    public static Font largeFont = new Font("Impact", Font.PLAIN, 40);
    public static Font levelUp = new Font("Impact", Font.BOLD, 60);
    public static Font controlFont = new Font("Impact", Font.BOLD, 40);
    public static Font highScoreFont = new Font("Arial", Font.BOLD, 19);
    public static Font highScoreFont2 = new Font("Impact", Font.BOLD, 19);
}
