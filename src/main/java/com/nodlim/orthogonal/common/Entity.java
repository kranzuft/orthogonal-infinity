package com.nodlim.orthogonal.common;

/**
 * Created by Aden James Prior
 * May 2014
 */

/**
 * - Entity -
 * Function: Entity super class for all objects,
 * holding common object functions and variables.
 */


public class Entity {
    protected double x, y, dx, dy; // coordinates of all objects

    // Call functions used by all objects
    public int x() { return (int) x; }
    public int y() { return (int) y; }
}
