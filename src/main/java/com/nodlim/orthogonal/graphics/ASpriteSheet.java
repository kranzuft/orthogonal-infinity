package com.nodlim.orthogonal.graphics;

import java.awt.*;

/**
 * Created by Aden James Prior
 * Apr 2014
 */

/**
 * - ASpriteSheet -
 * Sprite-Sheet dependent animation tool
 * that allows the incrementation through
 * a sprite sheet at a fixed rate.
 *
 * Initiated External Class Objects:
 *      sheet   Class: SpriteSheet
 */

public class ASpriteSheet {
    public SpriteSheet sheet;   // the sprite sheet used for the animation
    private boolean repeat;     // determines when to repeat the animation
    private int increment;      // determines current image to display

    // Constructor
    //
    // Parameters...
    // loc: directory of animation
    // gridSize: size of each image in animation.
    // repeat: value determining whether the animation is repeated
    public ASpriteSheet(String loc, int gridSize, boolean repeat) {
        sheet = new SpriteSheet(loc, gridSize);
        increment = 1;
        this.repeat = repeat;
    }

    // Get next image
    public Image Image(boolean active) {
        if (increment != -1) { // increments image until value is set to -1
            if (active) increment += 1;
            if (increment >= sheet.size() && repeat) increment = 1;
            else if (increment >= sheet.size()) {
                increment = -1;
                return null;
            }
            return sheet.get(increment); // returns relevant image
        } else {
            return null;
        }
    }
}
