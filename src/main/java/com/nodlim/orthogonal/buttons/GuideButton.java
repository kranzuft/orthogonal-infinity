package com.nodlim.orthogonal.buttons;

import com.nodlim.orthogonal.WindowInfo;
import com.nodlim.orthogonal.screens.Screens;

import java.awt.*;

/**
 * Controls Button
 * Used to see controls and options
 */

public class GuideButton extends com.nodlim.orthogonal.graphics.Button {
    public GuideButton(WindowInfo windowInfo, Image primary, Image secondary, Image primaryPressed, Image secondaryPressed) {
        super(windowInfo);
        setButtonImages(primary, secondary, primaryPressed, secondaryPressed);
        setOriginalCoordinates(windowInfo.windowX() * 5 / 12 - width() / 2);
        setText("Guide");
    }

    // Custom display
    // displays all information regarding the game relative to the frame bounds
    public void customDisplay(Graphics2D bd) {
        Screens.getHelpScreen(bd, windowInfo);
    }
}
